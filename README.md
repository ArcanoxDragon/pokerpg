PokeRPG is briman0094's game engine for Pokemon-styled RPG games. In order to compile, you should install OpenAL and OpenTK on your computer. Then, clone this repository as well as the ones listed below to the same parent folder and open "PokeRPG.sln".

Dependencies:

https://bitbucket.org/briman0094/gamedotnet - GameDotNet library (main game engine this project runs on)
https://bitbucket.org/briman0094/configlibrary - Briman's ConfigLibrary (handles game and world configuration files)

The project is designed to compile for the x86 platform as certain dependencies do not compile in a x64 environment, but the entire project will work fine on a 64-bit system. .NET 4.0 or higher is required.