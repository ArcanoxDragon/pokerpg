﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using GameDotNet;
using Ionic.Zip;

namespace PokeRPG
{
    public static class PackExtension
    {
        private static Dictionary<Pack, List<string>> _worldNames;
        private static Dictionary<Pack, string> _startWorldNames;

        static PackExtension()
        {
            _worldNames = new Dictionary<Pack, List<string>>();
            _startWorldNames = new Dictionary<Pack, string>();
        }

        public static bool InitializePack(this Pack pack)
        {
            if (pack.PackConfig.RootNode.HasKey("startWorld"))
            {
                string startWorld;
                pack.PackConfig.GetValue<string>("startWorld", out startWorld);
                _startWorldNames.Add(pack, startWorld);
                foreach (ZipEntry entry in pack.PackFile)
                {
                    if (entry.IsDirectory && entry.FileName.StartsWith("Worlds/") && entry.FileName != "Worlds/")
                    {
                        string[] pathComponents = entry.FileName.Split('/');
                        if (pathComponents.Length == 3) // "Worlds", world name, and blank (trailing slash)
                        {
                            _worldNames.Add(pack, new List<string>());
                            _worldNames[pack].Add(pathComponents[1]);
                        }
                    }
                }
                return true;
            }
            else
            {
                Console.WriteLine("Pack \"{0}\"'s config file does not have a startWorld attribute! Pack is not valid!", pack.Name);
                return false;
            }
        }

        public static string GetStartWorldName(this Pack pack)
        {
            if (!_startWorldNames.ContainsKey(pack))
                return null;

            return _startWorldNames[pack];
        }

        public static string[] GetFilenamesForWorld(this Pack pack, string worldName)
        {
            if (!_worldNames.ContainsKey(pack))
            {
                _worldNames.Add(pack, new List<string>());
            }

            if (!_worldNames[pack].Contains(worldName))
                return null;

            List<string> filenames = new List<string>();

            foreach (ZipEntry entry in pack.PackFile.Entries)
            {
                if (!entry.IsDirectory && entry.FileName.StartsWith(string.Format("Worlds/{0}/", worldName)))
                {
                    string[] pathComponents = entry.FileName.Split('/');
                    if (pathComponents.Length == 3) // "Worlds", world name, and filename
                    {
                        filenames.Add(pathComponents[2]);
                    }
                }
            }

            return filenames.ToArray();
        }

        public static Stream GetReadStreamForWorldAndChunk(this Pack pack, string worldName, int chunkX, int chunkY)
        {
            foreach (ZipEntry entry in pack.PackFile)
            {
                if (entry.FileName == string.Format("Worlds/{0}/{1}.{2}.wld", worldName, chunkX, chunkY))
                {
                    return entry.OpenReader();
                }
            }

            return null;
        }

        public static void WriteWorldChunkToPack(this Pack pack, string worldName, int chunkX, int chunkY, string chunkFileName)
        {
            if (!File.Exists(chunkFileName))
                return;

            for (int e = 0; e < pack.PackFile.Entries.Count; e++)
            {
                ZipEntry entry = pack.PackFile.Entries.ElementAt(e);
                if (entry.FileName == string.Format("Worlds/{0}/{1}.{2}.wld", worldName, chunkX, chunkY))
                {
                    Stream s = File.OpenRead(chunkFileName);
                    pack.PackFile.UpdateEntry(entry.FileName, s);
                    pack.PackFile.Save();
                    s.Close();
                    pack.LoadPack();
                    return;
                }
            }

            ZipEntry newEntry = pack.PackFile.AddFile(chunkFileName, string.Format("Worlds/{0}/", worldName));
            string newFileName = string.Format("Worlds/{0}/{1}.{2}.wld", worldName, chunkX, chunkY);
            if (newEntry.FileName != newFileName)
                newEntry.FileName = newFileName; // Doesn't like renaming to itself

            pack.PackFile.Save();
            pack.LoadPack();
        }

        public static string[] GetWorldNames(this Pack pack)
        {
            if (!_worldNames.ContainsKey(pack))
                _worldNames.Add(pack, new List<string>());

            return _worldNames[pack].ToArray<string>();
        }
    }
}
