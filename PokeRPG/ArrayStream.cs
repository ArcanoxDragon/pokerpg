﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PokeRPG
{
    public sealed class ArrayStream<T>
    {
        private T[] _array;
        private int _pos;

        public int Position
        {
            get
            {
                return this._pos;
            }
            set
            {
                if (value < 0 || value >= this._array.Length)
                    throw new ArgumentOutOfRangeException();
                this._pos = value;
            }
        }

        public bool EndOfStream
        {
            get
            {
                return this._pos >= this._array.Length - 1;
            }
        }

        public ArrayStream(T[] data)
        {
            this._array = data;
            this._pos = 0;
        }

        public T[] Read(int count)
        {
            if (this._pos + count > this._array.Length)
                throw new ArgumentOutOfRangeException("count");
            T[] ret = _array.Skip(this._pos).Take(count).ToArray<T>();
            this._pos += count;
            return ret;
        }

        public T[] ReadSafe(int count)
        {
            T[] ret = null;
            try
            {
                ret = this.Read(count);
            }
            catch
            {
                ret = null;
            }
            return ret;
        }
    }
}
