using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameDotNet.Graphics;
using OpenTK;

namespace PokeRPG.Effects
{
	public class Effect
    {

        protected delegate void EffectDelegate();
        protected event EffectDelegate OnAddedToWorld;
        protected event EffectDelegate OnRemovedFromWorld;

        public World World { get; private set; }
        public Vector3 Position { get; set; }

        public Effect()
        {
        }

        public void AddToWorld(World world)
        {
            this.World = world;
            if (this.OnAddedToWorld != null)
                this.OnAddedToWorld();
        }

        public void RemoveFromWorld()
        {
            if (this.OnRemovedFromWorld != null)
                this.OnRemovedFromWorld();
            this.World = null;
        }

        public virtual void Update()
        {

        }

        public virtual void Render(Shader defaultShader)
        {
            
        }
    }
}
