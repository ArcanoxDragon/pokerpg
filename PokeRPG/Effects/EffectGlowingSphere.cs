using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK;
using GameDotNet.Graphics;

namespace PokeRPG.Effects
{
    public sealed class EffectGlowingSphere : Effect
    {
        private float _radius;
        private Color4 _color;
        private Light _light;

        public EffectGlowingSphere(float radius, Color4 color)
            : base()
        {
            this._radius = radius;
            this._color = color;
            this._light = new Light();
            this._light.DiffuseColor = this._color.Multiply(4.0f);
            this._light.AttenuationConstant = 1.0f;
            this._light.AttenuationLinear = 1.0f;
            this._light.AttenuationQuadratic = 0.25f;
            this.OnAddedToWorld += new EffectDelegate(AddedToWorld);
            this.OnRemovedFromWorld += new EffectDelegate(RemovedFromWorld);
        }

        public Color4 Color { get { return this._color; } }
        public float Radius { get { return this._radius; } }

        void RemovedFromWorld()
        {
            this.World.RemoveLight(this._light);
        }

        void AddedToWorld()
        {
            this.World.AddLight(this._light);
            this.World.RenderEvent += WorldRenderEvent;
        }

        void WorldRenderEvent(EventPhase phase, double interval)
        {
            if (phase == EventPhase.Begin)
            {
                Vector4 lPos = this._light.Position;
                lPos.X = this.Position.X;
                lPos.Y = this.Position.Y;
                lPos.Z = this.Position.Z;
                lPos.W = 1.0f;
                this._light.Position = lPos;
            }
        }

        public override void Update()
        {
        }

        public override void Render(Shader defaultShader)
        {
            Shader shader = ShaderUtil.GetShader("Shaders/Effects/glowing_sphere");
            shader.Bind();

            ShaderUtil.SetUniform(shader, "color", this._color);
            ShaderUtil.SetUniform(shader, "radius", this._radius);

            //GL.DepthMask(false);
            Transform.PushMatrix();
            Transform.Scale(this._radius, this._radius, this._radius);
            Transform.Translate(this.Position.X, this.Position.Y, this.Position.Z);
            Transform.SendMatrices(shader);

            GL.Enable(EnableCap.Blend);
            GL.Enable(EnableCap.DepthTest);
            GL.BlendEquation(BlendEquationMode.FuncAdd);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            GL.DepthMask(false);
            Shape.SPHERE.Render(shader, 0);
            GL.DepthMask(true);

            Transform.PopMatrix();
        }
    }
}
