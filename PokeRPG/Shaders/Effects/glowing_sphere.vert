#version 150

in vec4 vertexPos;
in vec3 vertexNormal;
in vec2 vertexUV;

uniform mat4 modelView, modelViewProjection;
uniform mat4 normalTransform, uvTransform;

out vec4 vPos, vCenter;
out vec3 normal;

void main()
{
	vPos = modelView * vertexPos;
	vCenter = modelView * vec4(0.0, 0.0, 0.0, 1.0);
    normal = normalize(mat3(normalTransform) * vertexNormal);
	
	gl_Position = modelViewProjection * vertexPos;
}