#version 150

in vec4 vPos, vCenter;
in vec3 normal;

uniform vec4 color;
uniform float radius;

out vec4 fragmentColor, index;

void main()
{
	index = vec4(0.0, 0.0, 0.0, 1.0);

	vec4 tColor = color;
	float dist = length(vCenter.xy - vPos.xy);
	float intensity = 1.0 - (dist / radius);
	tColor += intensity * vec4(0.85);
	
	if (intensity > 0.25)
		tColor.a = 1.0;
	else if (intensity > 0.248)
		tColor.a = 0.75;
	else
		tColor.a = intensity * 2.0;

	tColor.a *= color.a;

	fragmentColor = tColor;
}