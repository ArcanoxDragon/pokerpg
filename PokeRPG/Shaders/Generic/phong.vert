#version 150

in vec4 vertexPos;
in vec3 vertexNormal;

uniform mat4 transform, modelView, modelViewProjection, depthVP;
uniform mat4 normalTransform;

out vec4 vPos, oPos, sPos;
out vec3 normal;

void main()
{
	oPos = transform * vertexPos;
	vPos = modelView * vertexPos;
    normal = normalize(mat3(normalTransform) * vertexNormal);
	
	gl_Position = modelViewProjection * vertexPos;
	sPos = depthVP * transform * vertexPos;
}


