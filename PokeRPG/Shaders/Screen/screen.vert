#version 150

in vec2 vertexUV;

out vec2 uv;
out vec4 vPos;

void main()
{
	uv = vertexUV;
	
	vPos = vec4(vertexUV.x * 2.0 - 1.0, vertexUV.y * 2.0 - 1.0, 0.0, 1.0);
	gl_Position = vec4(vertexUV.x * 2.0 - 1.0, vertexUV.y * 2.0 - 1.0, 0.0, 1.0);
}