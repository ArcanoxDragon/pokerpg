#version 150
#define NEAR 1.0
#define FAR  100.0
#define LOWER_THRESHOLD_D 0.000
#define UPPER_THRESHOLD_D 0.002
#define LOWER_THRESHOLD_I 0.000
#define UPPER_THRESHOLD_I 0.005

uniform int scrWidth, scrHeight;
uniform sampler2D fboColor;
uniform sampler2D fboIndex;
uniform sampler2D fboDepth;
uniform sampler2D fboGlow;
in vec2 uv;
in vec4 vPos;
out vec4 fragmentColor;

float linearizeDepth(float depth)
{
    return (NEAR * 2.0) / (FAR + NEAR - depth * (FAR - NEAR));
}

vec3 rgb2hsv(vec3 c)
{
    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
    vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));
    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;
    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

void main()
{
	float dX = dFdx(uv.x);
    float dY = dFdy(uv.y);
    int sX = int(uv.x * scrWidth);
    int sY = int(uv.y * scrHeight);
    float depthL = linearizeDepth(texture(fboDepth, uv - vec2(dX, 0.0)).r);
    float depthR = linearizeDepth(texture(fboDepth, uv + vec2(dX, 0.0)).r);
    float depthU = linearizeDepth(texture(fboDepth, uv - vec2(0.0, dY)).r);
    float depthD = linearizeDepth(texture(fboDepth, uv + vec2(0.0, dY)).r);
	float depthC = linearizeDepth(texture(fboDepth, uv).r);
    float indexL = texture(fboIndex, uv - vec2(dX, 0.0)).r;
    float indexR = texture(fboIndex, uv + vec2(dX, 0.0)).r;
    float indexU = texture(fboIndex, uv - vec2(0.0, dY)).r;
    float indexD = texture(fboIndex, uv + vec2(0.0, dY)).r;
    float dDdx = abs(depthR - depthL);
    float dDdy = abs(depthD - depthU);
    float dIdx = clamp(abs(indexR - indexL) * 256, 0.0, 1.0);
    float dIdy = clamp(abs(indexD - indexU) * 256, 0.0, 1.0);
    //float maxChangeD = max(dDdx, dDdy);
	//float maxChangeI = max(dIdx, dIdy);
	float maxChangeD = (dDdx + dDdy) / 2.0;
	float maxChangeI = (dIdx + dIdy) / 2.0;
	vec3 blurColor = vec3(0.0);
	int size = 4;
	for (int i = -size; i <= size; i += 1)
	{
		for (int j = -size; j <= size; j += 1)
		{
			vec4 _sample = texture(fboGlow, uv + vec2(float(i) * dX, float(j) * dY));
			//blurColor += mix(vec3(0.0), _sample.rgb * 2.0, _sample.a);
		}
	}
	blurColor /= pow(float(size) * 2.0 + 1.0, 2.0);
	vec3 diffColor = texture(fboColor, uv).rgb;
	//vec3 finalColor = mix(diffColor, blurColor, clamp((rgb2hsv(blurColor).z - 0.95) / 0.05, 0.0, 1.0));
	vec3 finalColor = clamp(diffColor + blurColor, 0.0, 1.0);
	float stepDepth = smoothstep(LOWER_THRESHOLD_D, UPPER_THRESHOLD_D, maxChangeD);
	float stepIndex = smoothstep(LOWER_THRESHOLD_I, UPPER_THRESHOLD_I, maxChangeI);
    vec3 tColor = mix(finalColor, vec3(0.0, 0.0, 0.0), max(stepDepth, stepIndex));
    fragmentColor = vec4(mix(finalColor, tColor, clamp(min(min(indexL, indexR), min(indexU, indexD)) * 1000.0, 0.0, 1.0)), 1.0);
}
