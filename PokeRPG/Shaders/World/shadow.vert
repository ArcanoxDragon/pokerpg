﻿in vec4 vertexPos;

uniform mat4 modelViewProjection;

void main()
{
	gl_Position = modelViewProjection * vertexPos;
}