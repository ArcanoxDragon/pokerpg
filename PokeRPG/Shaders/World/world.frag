#version 150
#define NEAR 1.0
#define FAR  100.0
#define SHADOW_SAMPLES 32.0
#define NORMAL_MAP_AMOUNT 0.05
#define MAX_LIGHTS 128 // Also defined in RenderUtil.cs!

in vec4 vPos, oPos, sPos;
in vec3 tangent, binormal, normal;
in mat3 tbn;
in vec2 vUV;

uniform int useBump, useToon, useGlow;
uniform int numLights, bufIndex;
uniform vec4 ambientGlobal;
uniform vec4 tintColor = vec4(1.0, 1.0, 1.0, 1.0);
uniform sampler2D tex, bump, glowMap, toonMap;
uniform sampler2D dirShadow;
uniform samplerCube[4] pointShadow;

struct Light
{
    vec4 position;
    vec4 diffuse;
	vec4 ambient;
	float attenConst;
	float attenLinear;
	float attenQuad;
	float shadow;
};

uniform lights
{
	Light light[MAX_LIGHTS];
};

out vec4 fragmentColor;
out vec4 objIndex;
out vec4 glow;

float linearizeDepth(float depth)
{
	return (NEAR * 2.0) / (FAR + NEAR - depth * (FAR - NEAR));
}

float rand(vec2 co)
{
    return fract(sin(dot(co.xy, vec2(12.9898, 78.233))) * 43758.5453);
}

void main()
{
	vec3 n, halfV, viewV, lightDir;
    float NdotL, NdotHV, realNdotL;
    vec4 lightCol = ambientGlobal;
	vec4 texCol = texture(tex, vUV);
	vec4 glowCol = vec4(0.0, 0.0, 0.0, 1.0);
    float att, dist;

	n = normalize(normal);

	if (!gl_FrontFacing)
		n = -n;

	vec3 bumpTex = texture(bump, vUV).rgb * 2.0 - vec3(1.0);
	bumpTex = tbn * (bumpTex * NORMAL_MAP_AMOUNT);

	n = normalize(mix(n, n + bumpTex, useBump));
	//n = normalize(n);

	for (int i = 0; i < numLights; i++)
	{
		if (abs(light[i].position.w - 1.0) < 1e-5) // Point light (w = 1.0)
		{
			lightDir = vec3(light[i].position - vPos);
			dist = length(lightDir);
			lightDir = normalize(lightDir);
			NdotL = clamp(dot(n, lightDir), 0.01, 0.99);

			realNdotL = mix(NdotL, texture(toonMap, vec2(0, NdotL)).r, float(useToon));

			float c = light[i].attenConst;
			float l = light[i].attenLinear;
			float q = light[i].attenQuad;

			att = clamp(1.0 / (c + l * dist + q * dist * dist), 0.0, 1.0);

			float depthVal = linearizeDepth(texture(pointShadow[int(light[i].shadow)], vPos.xyz));

			glowCol += smoothstep(0.95, 1.0, att) * light[i].diffuse * 0.5;

			lightCol += att * (light[i].diffuse * realNdotL + light[i].ambient);
		}
		else if (abs(light[i].position.w) < 1e-5) // Directional light (w = 0.0)
		{
			lightDir = -light[i].position.xyz;
			lightDir = normalize(lightDir);
			NdotL = clamp(dot(n, lightDir), 0.01, 0.99);
			realNdotL = mix(NdotL, texture(toonMap, vec2(0, NdotL)).r, float(useToon));
			
			float shadowVal = 0.0f;
			vec2 uvOffset;
			vec4 lDepth;
			for (float j = 0; j < SHADOW_SAMPLES; j += 1.0)
			{
				uvOffset = vec2(rand(oPos.xy + vec2(j / SHADOW_SAMPLES)), rand(sPos.xy + vec2(j / SHADOW_SAMPLES))) / 800;
				lDepth = texture(dirShadow, sPos.xy + uvOffset);
				shadowVal += step(lDepth.z + 3e-4, sPos.z) / SHADOW_SAMPLES;
			}

			realNdotL = mix(realNdotL, 0.0, clamp(shadowVal, 1e-5, 0.95) * light[i].shadow);

			lightCol += (light[i].diffuse * realNdotL + light[i].ambient);
		}
	}

	glowCol += mix(vec4(0.0), texture(glowMap, vUV), useGlow);
	glow = vec4(clamp(glowCol.rgb, 0.0, 1.0), 1.0);

	fragmentColor = lightCol * texCol * tintColor;
	//fragmentColor = lightCol * tintColor;
	objIndex = vec4(float(bufIndex) / 255.0, 0.0, 0.0, 1.0);
}
