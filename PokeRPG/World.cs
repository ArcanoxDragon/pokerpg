using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using PokeRPG.WorldObjects;
using PokeRPG.Effects;
using PokeRPG.Entities;
using Jitter;
using Jitter.Collision;
using Jitter.Dynamics;
using Jitter.LinearMath;
using JWorld = Jitter.World;
using GameDotNet.Graphics;
using GameDotNet;
using System.Threading;
using PokeRPG.ContentManager;

namespace PokeRPG
{
    public struct ChunkCoordinate : IEquatable<ChunkCoordinate>
    {
        private int _x, _y;

        public int X { get { return this._x; } }
        public int Y { get { return this._y; } }

        public ChunkCoordinate(int x, int y)
            : this()
        {
            this._x = x;
            this._y = y;
        }

        public bool Contains(Vector2 pos)
        {
            float wX = this._x * Chunk.CHUNK_SIZE;
            float wY = this._y * Chunk.CHUNK_SIZE;
            float wX2 = wX + Chunk.CHUNK_SIZE;
            float wY2 = wY + Chunk.CHUNK_SIZE;

            return (pos.X >= wX && pos.Y >= wY && pos.X <= wX2 && pos.Y <= wY2);
        }

        public static bool operator ==(ChunkCoordinate a, ChunkCoordinate b)
        {
            return a._x == b._x && a._y == b._y;
        }

        public static bool operator !=(ChunkCoordinate a, ChunkCoordinate b)
        {
            return a._x != b._x || a._y != b._y;
        }

        public Vector3 ToWorldCoordinates()
        {
            return new Vector3(this._x * Chunk.CHUNK_SIZE, this._y * Chunk.CHUNK_SIZE, 0.0f);
        }

        public override string ToString()
        {
            return string.Format("({0}, {1})", this.X, this.Y);
        }

        public bool Equals(ChunkCoordinate other)
        {
            return other._x == this._x && other._y == this._y;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is ChunkCoordinate))
                return false;
            return this.Equals((ChunkCoordinate) obj);
        }

        public override int GetHashCode()
        {
            int hash = 13;
            hash = (hash * 7) + _x.GetHashCode();
            hash = (hash * 7) + _y.GetHashCode();
            return hash;
        }
    }

    public sealed class Chunk
    {
        public const float CHUNK_SIZE = 16.0f;
        private List<WorldObject> _objects;
        private List<Light> _lights;

        public ChunkCoordinate Coordinates { get; private set; }

        public World ParentWorld { get; private set; }

        public Chunk(World parent, ChunkCoordinate coords)
        {
            this.Coordinates = coords;
            this._objects = new List<WorldObject>();
            this._lights = new List<Light>();
            this.ParentWorld = parent;
        }

        public void Update()
        {
            foreach (WorldObject o in this._objects)
            {
                o.Update();
            }
        }

        public int AddLights(ref Light[] lightArray, int startIndex)
        {
            int numLights = 0;
            for (int i = 0; i < this._lights.Count && i + startIndex < lightArray.Length; i++)
            {
                lightArray[i + startIndex] = this._lights[i];
                numLights++;
            }
            return numLights;
        }

        public void Render(Shader shader)
        {
            Transform.PushMatrix();
            foreach (WorldObject o in this._objects)
            {
                Transform.PushMatrix();
                o.Render(shader);
                Transform.PopMatrix();
            }
            Transform.PopMatrix();
        }

        public void AddObject(WorldObject obj)
        {
            this._objects.Add(obj);
            obj.AddToWorld(this.ParentWorld, this);
        }

        public void RemoveObject(WorldObject obj)
        {
            if (this._objects.Contains(obj))
            {
                obj.RemoveFromWorld();
                this._objects.Remove(obj);
            }
        }

        public List<T> GetObjects<T>(bool acceptSubclasses = true) where T : WorldObject
        {
            List<T> ret = new List<T>();

            foreach (WorldObject o in this._objects)
            {
                if (o.GetType() == typeof(T) || (acceptSubclasses && o.GetType().IsSubclassOf(typeof(T))))
                    ret.Add((T) o);
            }

            return ret;
        }

        public List<Light> GetLights()
        {
            return new List<Light>(this._lights);
        }

        public void AddLight(Light light)
        {
            this._lights.Add(light);
        }

        public void RemoveLight(Light light)
        {
            if (this._lights.Contains(light))
                this._lights.Remove(light);
        }
    }

    public sealed class World
    {
        // Statics
        private const int INGAME_SECONDS_PER_DAY = 24 * 60 * 60;
        private const double REAL_SECONDS_PER_GAME_HOUR = 30;
        private const double GAME_TIME_RATIO = (1.0 / REAL_SECONDS_PER_GAME_HOUR) * (60.0 * 60.0);
        private static Color4 SUN_DIFF_DAY = new Color4(0.8f * 1.0f, 0.8f * 0.8f, 0.8f * 0.5f, 1.0f);
        private static Color4 SUN_DIFF_NIGHT = new Color4(0.5f * 0.35f, 0.5f * 0.35f, 0.5f * 0.95f, 1.0f);
        private static Color4 SKY_COLOR = new Color4(0.4f, 0.4f, 1.0f, 0.0f);

        // Events
        public delegate void MouseClickEvent(Vector2 screenPos, Vector3 worldPos, int button, bool didHit, Entity hitEntity, WorldObject hitObject);
        public delegate void TimedPhasedEvent(EventPhase phase, double interval);
        public event MouseClickEvent OnMouseClick;
        public event Action<RigidBody, RigidBody> OnBeginCollide, OnEndCollide;
        public event Action<Entity> OnEntityAdded, OnEntityRemoved;
        public event TimedPhasedEvent RenderEvent, TickEvent;

        // Properties
        public Random Random { get; set; }

        public float CameraX { get; set; }

        public float CameraY { get; set; }

        public float CameraZ { get; set; }

        public float CameraZoom { get; set; }

        public float CameraElevation { get; set; }

        public double WorldTime { get; private set; }

        public JWorld PhysicsWorld { get; private set; }

        public string WorldName { get; private set; }

        private List<Chunk> _chunks;
        private List<Light> _gLights;
        private List<Effect> _effects;
        private List<Entity> _entities;
        private Light[] _tLights;
        private Light _sun;
        private Color4 _sky;
        private long _time;
        private long _lastPhysTick;

        // Input one-shots
        private bool _lastMouseL, _lastMouseR;

        // Time in in-game seconds
        public World(string worldName)
        {
            Console.WriteLine("Constructing world...");
            this.Random = new Random((int) GeneralUtil.MillisSinceEpoch());
            this.WorldName = worldName;
            this._chunks = new List<Chunk>();
            this._effects = new List<Effect>();
            this._entities = new List<Entity>();
            this._gLights = new List<Light>();
            this._tLights = new Light[GLUtil.MAX_LIGHTS];
            this._sun = new Light();
            this._sun.DiffuseColor = SUN_DIFF_DAY;
            this._sun.AmbientColor = new Color4(1.0f / 4.0f, 1.0f / 4.0f, 1.0f / 4.0f, 1.0f);
            this._sun.Position = new Vector4(-1.0f, -0.25f, -1.0f, 0.0f);
            this.CameraZoom = 4.0f;
            this.CameraElevation = 15.0f;
            this.PhysicsWorld = new JWorld(new CollisionSystemPersistentSAP());
            this.PhysicsWorld.Gravity = new JVector(0.0f, 0.0f, -9.8f);
            this.PhysicsWorld.Events.BodiesBeginCollide += OnPhysicsBeginCollide;
            this.PhysicsWorld.Events.BodiesEndCollide += OnPhysicsEndCollide;
            this.PhysicsWorld.ContactSettings.AllowedPenetration = 0.001f;
            this._time = (long) ((16.0 / 24.0) * INGAME_SECONDS_PER_DAY); // 9:00 AM
            this._sky = SKY_COLOR;
            this._lastPhysTick = GeneralUtil.MillisSinceEpoch();
        }

        public List<Chunk> GetChunks()
        {
            List<Chunk> ret = new List<Chunk>();

            foreach (Chunk c in this._chunks)
                ret.Add(c);

            return ret;
        }

        public Chunk GetChunk(ChunkCoordinate chunkCoord)
        {
            for (int c = 0; c < this._chunks.Count; c++)
            {
                Chunk ch = this._chunks[c];
                if (ch.Coordinates == chunkCoord)
                    return ch;
            }
            return null;
        }

        public void AddChunk(Chunk chunk)
        {
            if (this.GetChunk(chunk.Coordinates) != null)
                throw new ArgumentException(string.Format("Chunk ({0}, {1}) already exists and cannot be re-added without first being removed!", chunk.Coordinates.X, chunk.Coordinates.Y));

            lock (this._chunks)
            {
                this._chunks.Add(chunk);
            }
        }

        private void OnPhysicsBeginCollide(RigidBody a, RigidBody b)
        {
            if (this.OnBeginCollide != null)
                this.OnBeginCollide(a, b);
        }

        private void OnPhysicsEndCollide(RigidBody a, RigidBody b)
        {
            if (this.OnEndCollide != null)
                this.OnEndCollide(a, b);
        }

        public void AddLight(Light light)
        {
            lock (this._gLights)
            {
                this._gLights.Add(light);
            }
        }

        public void RemoveLight(Light light)
        {
            if (this._gLights.Contains(light))
                lock (this._gLights)
                {
                    this._gLights.Remove(light);
                }
            foreach (Chunk c in this._chunks)
                c.RemoveLight(light);
        }

        public void AddPhysicsBody(RigidBody body)
        {
            this.PhysicsWorld.AddBody(body);
        }

        public void RemovePhysicsBody(RigidBody body)
        {
            this.PhysicsWorld.RemoveBody(body);
        }

        public void AddEntity(Entity entity)
        {
            entity.AddToWorld(this);
            this._entities.Add(entity);

            if (this.OnEntityAdded != null)
                this.OnEntityAdded(entity);
        }

        public void RemoveEntity(Entity entity)
        {
            if (this._entities.Contains(entity))
            {
                entity.RemoveFromWorld();
                this._entities.Remove(entity);

                if (this.OnEntityRemoved != null)
                    this.OnEntityRemoved(entity);
            }
        }

        public List<T> GetEntities<T>(bool acceptSubclasses = true) where T : Entity
        {
            List<T> ret = new List<T>();

            foreach (Entity e in this._entities)
            {
                if (e.GetType() == typeof(T) || (acceptSubclasses && e.GetType().IsSubclassOf(typeof(T))))
                    ret.Add((T) e);
            }

            return ret;
        }

        public List<T> GetObjects<T>(bool acceptSubclasses = true) where T : WorldObject
        {
            List<T> ret = new List<T>();

            foreach (Chunk c in this._chunks)
            {
                ret.AddRange(c.GetObjects<T>(acceptSubclasses));
            }

            return ret;
        }

        public void AddEffect(Effect effect)
        {
            effect.AddToWorld(this);
            this._effects.Add(effect);
        }

        public void RemoveEffect(Effect effect)
        {
            if (this._effects.Contains(effect))
            {
                effect.RemoveFromWorld();
                this._effects.Remove(effect);
            }
        }

        public List<T> GetEffects<T>(bool acceptSubclasses = true) where T : Effect
        {
            List<T> ret = new List<T>();

            foreach (Effect e in this._effects)
            {
                if (e.GetType() == typeof(T) || (acceptSubclasses && e.GetType().IsSubclassOf(typeof(T))))
                    ret.Add((T) e);
            }

            return ret;
        }

        /// <summary>
        /// This method is where effects render. They are able to assign their own shaders.
        /// </summary>
        public void RenderEffects(double interval, Shader def)
        {
            List<Effect> tEffects = new List<Effect>(this._effects);
            Vector3 camPos = new Vector3(this.CameraX, this.CameraY, this.CameraZ);
            tEffects.Sort((ef1, ef2) =>
            {
                double len1 = (ef1.Position - camPos).Length;
                double len2 = (ef2.Position - camPos).Length;
                return len2.CompareTo(len1);
            });

            def.Bind();
            for (int i = 0; i < tEffects.Count; i++)
            {
                tEffects[i].Render(def);
                def.Bind();
            }
        }

        public int SetupLights(ref Light[] lights)
        {
            lights[0] = this._sun;
            this._gLights.CopyTo(lights, 1);
            int numLights = this._gLights.Count + 1;

            foreach (Chunk c in this._chunks)
            {
                numLights += c.AddLights(ref lights, numLights);
            }

            return numLights;
        }

        /// <summary>
        /// This method is where normal objects render. They will use the main shader.
        /// </summary>
        public void RenderObjects(double interval, Shader shader, bool firstPass = true, bool useShadow = true, bool setCamera = true)
        {
            GL.ClearColor(this._sky);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            if (firstPass && this.RenderEvent != null)
                this.RenderEvent(EventPhase.Begin, interval);

            if (setCamera)
            {
                RenderUtil.SetView(this.CameraZoom);
                RenderUtil.SetCamera(this.CameraX, this.CameraY, this.CameraZ, this.CameraElevation);
            }

            if (firstPass)
            {
                Matrix4 tProj = Transform.GetInstance().Projection;
                Matrix4 tView = Transform.GetInstance().View;
                int numLights = SetupLights(ref this._tLights);

                if (useShadow)
                {
                    this._sun.Shadow = 1.0f;
                    ShaderUtil.GetShader(WorldState.SHADOW_SHADER_NAME).Bind();
                    this._sun.SetTransformForShadowRender();
                    this._sun.BeginShadowRender();
                    this.RenderObjects(interval, Shader.CurrentShader, firstPass: false, setCamera: false);
                    this._sun.FinalizeShadowRender();
                    shader.Bind();
                    GL.ActiveTexture(TextureUnit.Texture10);
                    this._sun.ShadowBuffer.BindDepthTexture();
                }

                Transform.SetProjection(tProj);
                Transform.SetView(tView);

                ShaderUtil.SetUniform(shader, "numLights", numLights);
                GLUtil.ApplyLights(ref this._tLights);
                GLUtil.BindLightBuffer(shader);
                ShaderUtil.SetUniform(shader, "ambientGlobal", this._sun.AmbientColor);

                ShaderUtil.SetUniform(shader, "dirShadow", 10); // Active texture 10 is shadow texture
            }

            for (int i = 0; i < this._entities.Count; i++)
                this._entities[i].Render(shader);

            lock (this._chunks)
            {
                foreach (Chunk c in this._chunks)
                {
                    Transform.PushMatrix();
                    c.Render(shader);
                    Transform.PopMatrix();
                }
            }

            if (firstPass && this.RenderEvent != null)
                this.RenderEvent(EventPhase.End, interval);
        }

        public void Update(double interval)
        {
            if (this.TickEvent != null)
                this.TickEvent(EventPhase.Begin, interval);

            foreach (Chunk c in this._chunks)
                c.Update();
            for (int i = 0; i < this._entities.Count; i++)
                this._entities[i].Update();
            foreach (Effect e in this._effects)
                e.Update();

            if (Launcher.GameInstance.Window.Mouse[OpenTK.Input.MouseButton.Left])
            {
                if (!this._lastMouseL)
                {
                    this._lastMouseL = true;
                    if (this.OnMouseClick != null)
                    {
                        JVector camCoords = new JVector(this.CameraX, this.CameraY, this.CameraZ + this.CameraElevation);
                        JVector camDir = (new JVector(this.CameraX, this.CameraY + 25.0f, this.CameraZ) - camCoords);
                        camDir.Normalize();
                        RigidBody hitBody;
                        JVector hitNormal;
                        float distance;
                        bool hit = this.PhysicsWorld.CollisionSystem.Raycast(camCoords, camDir, null, out hitBody, out hitNormal, out distance);
                        Vector2 screenPos = new Vector2(Launcher.GameInstance.Window.Mouse.X, Launcher.GameInstance.Window.Mouse.Y);
                        JVector hitPos = camCoords + (camDir * distance);
                        Vector3 worldPos = new Vector3(hitPos.X, hitPos.Y, hitPos.Z);
                        this.OnMouseClick(screenPos, worldPos, 0, hit, null, null);
                    }
                }
            }
            else
            {
                this._lastMouseL = false;
            }

            // Time
            /*if (Launcher.GameInstance.Window.Keyboard[OpenTK.Input.Key.Tab])
                this._time += (int) Math.Round(GAME_TIME_RATIO * Launcher.GameInstance.TickPeriod) * 20;
            else
                this._time += (int) Math.Round(GAME_TIME_RATIO * Launcher.GameInstance.TickPeriod);*/
            // 0.0 - 1.0; 0.5 = noon
            this.WorldTime = (double) (this._time % INGAME_SECONDS_PER_DAY) / (double) INGAME_SECONDS_PER_DAY;
            Vector4 sunPos = this._sun.Position;
            double angle = (this.WorldTime * MathHelper.TwoPi) - MathHelper.PiOver2;
            if (angle > MathHelper.TwoPi)
                angle -= MathHelper.TwoPi;
            if (angle < 0.0f)
                angle += MathHelper.TwoPi;
            sunPos.Z = (float) -Math.Abs(Math.Sin(angle));
            if (angle >= MathHelper.Pi) // Night time
            {
                sunPos.X = (float) Math.Cos(angle);
                this._sun.DiffuseColor = SUN_DIFF_NIGHT.Multiply(-sunPos.Z);
                this._sky = Color4.Black;
            }
            else // Daytime
            {
                sunPos.X = (float) -Math.Cos(angle);
                this._sun.DiffuseColor = SUN_DIFF_DAY.Multiply(-sunPos.Z * 2.0f);
                this._sky = SKY_COLOR.Multiply(-sunPos.Z);
            }
            this._sun.Position = sunPos;
            this._sun.AmbientColor = this._sun.DiffuseColor.Multiply(0.25f);

            // Physics
            if (interval < 0.055)
                this.PhysicsWorld.Step((float) interval, true);

            if (this.TickEvent != null)
                this.TickEvent(EventPhase.End, interval);
        }

        public void ConvertWorld()
        {
            foreach (string fileName in Directory.EnumerateFiles("World/", "*.*.wold"))
            {
                int cX, cY;
                string[] split = fileName.Substring(fileName.IndexOf('/') + 1).Split('.');
                if (split.Length == 3)
                {
                    string sX = split[0];
                    string sY = split[1];
                    if (int.TryParse(sX, out cX) && int.TryParse(sY, out cY))
                    {
                        Console.WriteLine("Building chunk at ({0}, {1}) from {2}", cX, cY, fileName);
                        ChunkCoordinate cc = new ChunkCoordinate(cX, cY);
                        Chunk c = new Chunk(this, cc);
                        string[] lines = File.ReadAllLines(fileName);
                        string newFileName = string.Format("World/{0}.{1}.wld", sX, sY);
                        StreamWriter w = new StreamWriter(new FileStream(newFileName, FileMode.Create, FileAccess.Write, FileShare.Read), Encoding.Default);
                        w.AutoFlush = true;
                        foreach (string line in lines)
                        {
                            string[] pars = line.Split(',');
                            if (pars.Length > 0)
                            {
                                short type;
                                if (short.TryParse(pars[0], out type))
                                {
                                    w.Write(Encoding.Default.GetChars(BitConverter.GetBytes(type)), 0, 2);
                                    switch (Math.Abs(type))
                                    {
                                        case 0: // Scenery
                                            if (pars.Length == 5)
                                            {
                                                float oX, oY, oZ;
                                                if (float.TryParse(pars[1], out oX) && float.TryParse(pars[2], out oY) && float.TryParse(pars[3], out oZ))
                                                {
                                                    // 3 floats to write; length == 4 * 3 or 12
                                                    byte[] o = new byte[12];
                                                    BitConverter.GetBytes(oX).CopyTo(o, 0);
                                                    BitConverter.GetBytes(oY).CopyTo(o, 4);
                                                    BitConverter.GetBytes(oZ).CopyTo(o, 8);
                                                    w.Write(Encoding.Default.GetChars(BitConverter.GetBytes((short) (o.Length + pars[4].Length + 1))), 0, 2); // Write length (3 4-byte floats, then the string, then the string's null-terminator)
                                                    w.Write(Encoding.Default.GetChars(o), 0, o.Length);
                                                    w.Write(pars[4]);
                                                    w.Write('\0');
                                                }
                                            }
                                            break;
                                        case 1: // Ground
                                            if (pars.Length == 7)
                                            {
                                                float gX, gY, gZ;
                                                float gW, gH;
                                                if (float.TryParse(pars[1], out gX) && float.TryParse(pars[2], out gY) && float.TryParse(pars[3], out gZ) && float.TryParse(pars[4], out gW) && float.TryParse(pars[5], out gH))
                                                {
                                                    // 5 floats to write; length == 4 * 5 or 20
                                                    byte[] o = new byte[20];
                                                    BitConverter.GetBytes(gX).CopyTo(o, 0);
                                                    BitConverter.GetBytes(gY).CopyTo(o, 4);
                                                    BitConverter.GetBytes(gZ).CopyTo(o, 8);
                                                    BitConverter.GetBytes(gW).CopyTo(o, 12);
                                                    BitConverter.GetBytes(gH).CopyTo(o, 16);
                                                    w.Write(Encoding.Default.GetChars(BitConverter.GetBytes((short) (o.Length + pars[6].Length + 1))), 0, 2); // Write length (5 4-byte floats, then the string, then the string's null-terminator)
                                                    w.Write(Encoding.Default.GetChars(o), 0, o.Length);
                                                    w.Write(pars[6]);
                                                    w.Write('\0');
                                                }
                                            }
                                            break;
                                        case 2: // Character
                                            if (pars.Length == 5)
                                            {
                                                float oX, oY, oZ;
                                                if (float.TryParse(pars[1], out oX) && float.TryParse(pars[2], out oY) && float.TryParse(pars[3], out oZ))
                                                {
                                                    // 3 floats to write; length == 4 * 3 or 12
                                                    byte[] o = new byte[12];
                                                    BitConverter.GetBytes(oX).CopyTo(o, 0);
                                                    BitConverter.GetBytes(oY).CopyTo(o, 4);
                                                    BitConverter.GetBytes(oZ).CopyTo(o, 8);
                                                    w.Write(Encoding.Default.GetChars(BitConverter.GetBytes((short) (o.Length + pars[4].Length + 1))), 0, 2); // Write length (3 4-byte floats, then the string, then the string's null-terminator)
                                                    w.Write(Encoding.Default.GetChars(o), 0, o.Length);
                                                    w.Write(pars[4]);
                                                    w.Write('\0');
                                                }
                                            }
                                            break;
                                        case 3: // Light
                                            if (pars.Length == 12)
                                            {
                                                float lX, lY, lZ, lW;
                                                float dR, dG, dB;
                                                float aR, aG, aB;
                                                float aD;
                                                if (float.TryParse(pars[1], out lX) && float.TryParse(pars[2], out lY) && float.TryParse(pars[3], out lZ) && float.TryParse(pars[4], out lW) && float.TryParse(pars[5], out dR) && float.TryParse(pars[6], out dG) && float.TryParse(pars[7], out dB) && float.TryParse(pars[8], out aR) && float.TryParse(pars[9], out aG) && float.TryParse(pars[10], out aB) && float.TryParse(pars[11], out aD))
                                                {
                                                    // 11 floats to write; length == 4 * 11 or 44
                                                    byte[] o = new byte[44];
                                                    BitConverter.GetBytes(lX).CopyTo(o, 0);
                                                    BitConverter.GetBytes(lY).CopyTo(o, 4);
                                                    BitConverter.GetBytes(lZ).CopyTo(o, 8);
                                                    BitConverter.GetBytes(lW).CopyTo(o, 12);
                                                    BitConverter.GetBytes(dR).CopyTo(o, 16);
                                                    BitConverter.GetBytes(dG).CopyTo(o, 20);
                                                    BitConverter.GetBytes(dB).CopyTo(o, 24);
                                                    BitConverter.GetBytes(aR).CopyTo(o, 28);
                                                    BitConverter.GetBytes(aG).CopyTo(o, 32);
                                                    BitConverter.GetBytes(aB).CopyTo(o, 36);
                                                    BitConverter.GetBytes(aD).CopyTo(o, 40);
                                                    w.Write(Encoding.Default.GetChars(BitConverter.GetBytes((short) (o.Length))), 0, 2); // Write length (11 4-byte floats)
                                                    w.Write(Encoding.Default.GetChars(o), 0, o.Length);
                                                }
                                            }
                                            break;
                                    }
                                }
                            }
                        }
                        w.Close();
                        this._chunks.Add(c);
                    }
                }
            }
        }

        public void LoadWorld()
        {
            bool foundChunks = false;
            WorldDataTypeRegistry.Instance.SearchForDataHandlers();

            if (Directory.Exists(Path.Combine("Worlds", this.WorldName)))
            {
                foreach (string fileName in Directory.EnumerateFiles(Path.Combine("Worlds", this.WorldName)))
                {
                    int cX, cY;
                    string[] split = fileName.Split('.');
                    if (split.Length == 3)
                    {
                        string sX = split[0];
                        string sY = split[1];
                        if (int.TryParse(sX, out cX) && int.TryParse(sY, out cY))
                        {
                            Console.WriteLine("Building chunk at ({0}, {1}) from file \"{2}\"", cX, cY, fileName);
                            ChunkCoordinate cc = new ChunkCoordinate(cX, cY);
                            Stream chunkStream = File.OpenRead(fileName);
                            if (chunkStream != null)
                            {
                                foundChunks = true;
                                this._chunks.Add(LoadChunk(cc, chunkStream));
                                chunkStream.Close();
                            }
                            else
                            {
                                Console.WriteLine("An error occurred while opening a stream for chunk ({0}, {1}) for world named \"{2}\"", cX, cY, this.WorldName);
                            }
                        }
                    }
                }
            }

            Pack pack = Game.Instance.Pack;
            if (pack.GetWorldNames().Contains(this.WorldName))
            {
                foreach (string fileName in pack.GetFilenamesForWorld(this.WorldName))
                {
                    int cX, cY;
                    string[] split = fileName.Split('.');
                    if (split.Length == 3)
                    {
                        string sX = split[0];
                        string sY = split[1];
                        if (int.TryParse(sX, out cX) && int.TryParse(sY, out cY))
                        {
                            Console.WriteLine("Building chunk at ({0}, {1}) from pack file \"{2}\"", cX, cY, fileName);
                            ChunkCoordinate cc = new ChunkCoordinate(cX, cY);
                            Stream chunkStream = pack.GetReadStreamForWorldAndChunk(this.WorldName, cX, cY);
                            if (chunkStream != null)
                            {
                                foundChunks = true;
                                this._chunks.Add(LoadChunk(cc, chunkStream));
                            }
                            else
                            {
                                Console.WriteLine("An error occurred while opening a pack stream for chunk ({0}, {1}) for world named \"{2}\"", cX, cY, this.WorldName);
                            }
                        }
                    }
                }
            }

            if (!foundChunks)
            {
                Console.WriteLine("Could not find any chunks for world \"{0}\"!", this.WorldName);
            }
        }

        private Chunk LoadChunk(ChunkCoordinate coords, Stream inputStream)
        {
            Chunk c = new Chunk(this, coords);

            byte[] chunkData = new byte[inputStream.Length];
            inputStream.Read(chunkData, 0, chunkData.Length);
            ArrayStream<byte> r = new ArrayStream<byte>(chunkData);
            while (!r.EndOfStream)
            {
                byte[] bID = r.Read(2);
                byte[] bLen = r.Read(2);
                short id = BitConverter.ToInt16(bID, 0);
                short len = BitConverter.ToInt16(bLen, 0);
                try
                {
                    LoadWorldObjectHandler loadHandler = WorldDataTypeRegistry.Instance.GetLoadHandler(id);
                    if (loadHandler != null)
                    {
#if VERBOSE
                                    Console.WriteLine("Loading object with ID {0} using load handler \"{1}\"...", id, loadTypes[id].Method.Name);
#endif
                        byte[] loadedData = r.Read(len);
                        byte[] data;
                        if (WorldDataTypeRegistry.Instance.GetTypeForId(id).IsSubclassOf(typeof(WorldObject)))
                            data = loadedData.SubArray(16, loadedData.Length - 16);
                        else
                            data = loadedData;
                        object loaded = loadHandler(this, c, data);
                        if (loaded is WorldObject)
                        {
                            WorldObject obj = (WorldObject) loaded;
                            obj.ObjectGuid = new Guid(data.Take(16).ToArray());
                            c.AddObject(obj);
                        }
                        else if (loaded is Entity)
                        {
                            this.AddEntity((Entity) loaded);
                        }
                        else if (loaded is Light)
                        {
                            c.AddLight((Light) loaded);
                        }
                    }
                    else
                    {
                        Console.WriteLine("No load handler registered for load ID \"{0}\"", id);
                        r.Read(len);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }

            return c;
        }

        public void SaveWorld()
        {
            WorldDataTypeRegistry.Instance.SearchForDataHandlers();

            foreach (Chunk chunk in this._chunks)
            {
                SaveChunk(chunk);
            }
        }

        private void SaveChunk(Chunk chunk)
        {
            string filePath = Path.Combine("Worlds", this.WorldName, string.Format("{0}.{1}.wld", chunk.Coordinates.X, chunk.Coordinates.Y));
            bool fileAlreadyExists = File.Exists(filePath);
            if (!fileAlreadyExists)
                Directory.CreateDirectory(Path.GetDirectoryName(filePath));

            Stream s = File.Open(filePath, FileMode.Create, FileAccess.Write, FileShare.Read);
            foreach (WorldObject obj in chunk.GetObjects<WorldObject>())
            {
                try
                {
                    SaveWorldItemHandler handler = WorldDataTypeRegistry.Instance.GetSaveHandler(obj.GetType());
                    if (handler != null)
                    {
                        int id = WorldDataTypeRegistry.Instance.GetIdForType(obj.GetType());
                        List<byte> dataList = new List<byte>(handler(this, chunk, obj));
                        dataList.InsertRange(0, obj.ObjectGuid.ToByteArray());
                        byte[] data = dataList.ToArray();
                        int len = data.Length;
                        byte[] bID = BitConverter.GetBytes((short) id);
                        byte[] bLen = BitConverter.GetBytes((short) len);
                        s.Write(bID, 0, bID.Length);
                        s.Write(bLen, 0, bLen.Length);
                        s.Write(data, 0, len);
                    }
                    else
                    {
                        Console.WriteLine("No save handler registered for data type \"{0}\"", obj.GetType().Name);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }

            foreach (Light light in chunk.GetLights())
            {
                try
                {
                    SaveWorldItemHandler handler = WorldDataTypeRegistry.Instance.GetSaveHandler(light.GetType());
                    if (handler != null)
                    {
                        int id = WorldDataTypeRegistry.Instance.GetIdForType(light.GetType());
                        byte[] data = handler(this, chunk, light);
                        int len = data.Length;
                        byte[] bID = BitConverter.GetBytes((short) id);
                        byte[] bLen = BitConverter.GetBytes((short) len);
                        s.Write(bID, 0, bID.Length);
                        s.Write(bLen, 0, bLen.Length);
                        s.Write(data, 0, len);
                    }
                    else
                    {
                        Console.WriteLine("No save handler registered for data type \"{0}\"", light.GetType().Name);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }

            foreach (Entity ent in this._entities.Where((e) => chunk.Coordinates.Contains(new Vector2(e.PhysicsBody.Position.X, e.PhysicsBody.Position.Y))))
            {
                try
                {
                    SaveWorldItemHandler handler = WorldDataTypeRegistry.Instance.GetSaveHandler(ent.GetType());
                    if (handler != null)
                    {
                        int id = WorldDataTypeRegistry.Instance.GetIdForType(ent.GetType());
                        byte[] data = handler(this, chunk, ent);
                        int len = data.Length;
                        byte[] bID = BitConverter.GetBytes((short) id);
                        byte[] bLen = BitConverter.GetBytes((short) len);
                        s.Write(bID, 0, bID.Length);
                        s.Write(bLen, 0, bLen.Length);
                        s.Write(data, 0, len);
                    }
                    else
                    {
                        Console.WriteLine("No save handler registered for data type \"{0}\"", ent.GetType().Name);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
            s.Close();

            if (!fileAlreadyExists) // We must be saving to the current pack
            {
                Pack p = Game.Instance.Pack;
                p.WriteWorldChunkToPack(this.WorldName, chunk.Coordinates.X, chunk.Coordinates.Y, filePath);
                File.Delete(filePath);
            }
        }
    }
}
