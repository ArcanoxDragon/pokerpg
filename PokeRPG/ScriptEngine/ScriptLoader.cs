using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using CSScriptLibrary;
using Mono.CSharp;
using System.Reflection;
using GameDotNet;

namespace PokeRPG.ScriptEngine
{
    public class ScriptCompileException : Exception
    {
        private string _scriptPath;
        private string _message;

        public ScriptCompileException(string scriptPath, string message)
        {
            this._scriptPath = scriptPath;
            this._message = message;
        }

        public override string Message
        {
            get
            {
                return string.Format("Script \"{0}\" failed to compile: {1}", this._scriptPath, this._message);
            }
        }
    }

    public class ScriptLoader
    {
        #region Static
        private static readonly List<string> _imports;

        static ScriptLoader()
        {
            _imports = new List<string>();

            _imports.Add("System");
            _imports.Add("System.Collections.Generic");
            _imports.Add("System.Linq");
            _imports.Add("System.Text");
            _imports.Add("PokeRPG");
            _imports.Add("PokeRPG.Effects");
            _imports.Add("PokeRPG.Entities");
            _imports.Add("PokeRPG.WorldObjects");
            _imports.Add("PokeRPG.ScriptEngine");
        }
        #endregion

        private Dictionary<string, ScriptedObject> _loadedScripts;

        public ScriptLoader()
        {
            this._loadedScripts = new Dictionary<string, ScriptedObject>();
        }

        public T LoadClass<T>(string scriptPath) where T : ScriptedObject, new()
        {
            if (this._loadedScripts.ContainsKey(scriptPath))
                return (T) this._loadedScripts[scriptPath];

            string scriptSource = null;

            if (File.Exists(scriptPath))
            {
                scriptSource = File.ReadAllText(Path.GetFullPath(scriptPath));
            }
            else
            {
                Pack curPack = Launcher.GameInstance.Pack;
                if (curPack.PackFile[scriptPath] != null)
                {
                    Stream s = curPack.PackFile[scriptPath].OpenReader();
                    byte[] sData = new byte[s.Length];
                    s.Read(sData, 0, sData.Length);
                    s.Close();
                    scriptSource = Encoding.UTF8.GetString(sData);
                }
            }

            if (scriptSource != null)
            {
                string header = "";
                string footer = "";
                foreach (string import in _imports)
                    header += string.Format("using {0};\n", import);
                header += "namespace PokeRPG.Scripts {";
                footer += "}";
                string script = header + scriptSource + footer;

                Assembly a = CSScript.LoadCode(script);
                T loadedClass = null;

                if (a.GetLoadedModules().Length > 0)
                {
                    if (a.GetLoadedModules()[0].GetTypes().Length > 0)
                    {
                        try
                        {
                            loadedClass = (T) a.CreateInstance(a.GetLoadedModules()[0].GetTypes()[0].FullName);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Could not cast script from \"{0}\" to \"{1}\":\n", a.GetLoadedModules()[0].GetTypes()[0].Name, typeof(T).Name);
                            Console.WriteLine(ex.ToString());
                        }
                    }
                }

                if (loadedClass == null || !loadedClass.GetType().IsSubclassOf(typeof(ScriptedObject)))
                {
                    throw new ScriptCompileException(Path.GetFullPath(scriptPath), "Could not instantiate type");
                }

                this._loadedScripts.Add(scriptPath, loadedClass);
                return loadedClass;
            }
            else
            {
                Console.WriteLine("Could not find script file \"{0}\"!", scriptPath);
            }

            return null;
        }
    }
}
