﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PokeRPG.Entities;

namespace PokeRPG.ScriptEngine
{
    public class ScriptedEntity : ScriptedObject
    {
        protected Entity ThisEntity { get; private set; }

        protected virtual void OnUpdate() { }
        protected virtual void OnRender() { }
        protected virtual void OnAddedToWorld() { }
        protected virtual void OnRemovedFromWorld() { }
        protected virtual void OnCollidedWith(ScriptedObject other) { }
    }
}
