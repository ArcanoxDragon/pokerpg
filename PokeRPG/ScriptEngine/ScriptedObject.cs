﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameDotNet;

namespace PokeRPG.ScriptEngine
{
    public class ScriptedObject
    {
        #region Helper Properties
        protected Game Game
        {
            get
            {
                return Launcher.GameInstance;
            }
        }

        protected World World
        {
            get
            {
                if (Game.CurrentState is WorldState)
                {
                    return ((WorldState) Game.CurrentState).World; 
                }
                return null;
            }
        }
        #endregion Helper Properties

        public virtual void Update()
        {

        }

        public virtual void Render()
        {

        }
    }
}
