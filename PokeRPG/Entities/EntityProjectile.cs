﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PokeRPG;
using PokeRPG.Effects;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Jitter;
using Jitter.Collision;
using Jitter.Collision.Shapes;
using Jitter.Dynamics;
using Jitter.LinearMath;

namespace PokeRPG.Entities
{
	public class EntityProjectile : Entity
	{
		private EffectGlowingSphere _ef;
		private long _born, _lifetime;
		private float _damage;
		private SphereShape _shape;

		public EntityProjectile(float radius, Color4 color, long lifetime, float damage)
        {
			this._ef = new EffectGlowingSphere(radius, color);
			this._born = GeneralUtil.MillisSinceEpoch();
			this._lifetime = lifetime;
			this._shape = new SphereShape(radius);
			this._damage = damage;
			this.PhysicsBody = new RigidBody(this._shape);
			this.PhysicsBody.AffectedByGravity = false;
			this.PhysicsBody.Material.Restitution = 2.0f;
			this.PhysicsBody.Mass = 1.0f;
			this.PhysicsBody.Tag = this;
			this.OnAddedToWorld += AddedToWorld;
			this.OnRemovedFromWorld += RemovedFromWorld;
        }

		void RemovedFromWorld(World world)
		{
			this.World.RemoveEffect(this._ef);
		}

		void AddedToWorld(World world)
		{
			this.World.AddEffect(this._ef);
			this.World.OnBeginCollide += OnCollision;
            this.World.RenderEvent += WorldRenderEvent;
		}

        void WorldRenderEvent(EventPhase phase, double interval)
        {
            if (phase == EventPhase.Begin)
            {
                Vector3 efPos = this._ef.Position;
                efPos.X = this.PhysicsBody.Position.X;
                efPos.Y = this.PhysicsBody.Position.Y;
                efPos.Z = this.PhysicsBody.Position.Z;
                this._ef.Position = efPos;
            }
        }

        public void SetPosition(float x, float y, float z)
        {
            this.PhysicsBody.Position = new JVector(x, y, z);
            Vector3 efPos = this._ef.Position;
            efPos.X = this.PhysicsBody.Position.X;
            efPos.Y = this.PhysicsBody.Position.Y;
            efPos.Z = this.PhysicsBody.Position.Z;
            this._ef.Position = efPos;
        }

		public override void Update()
		{
			base.Update();

			if ((GeneralUtil.MillisSinceEpoch() - this._born) > this._lifetime)
			{
				if (this.World != null)
				{
					this.World.RemoveEffect(this._ef);
					this.World.RemoveEntity(this);
				}
			}
		}

		void OnCollision(RigidBody a, RigidBody b)
		{
			if (a.Tag == this && b.Tag is EntityLiving)
			{
				EntityLiving other = (EntityLiving) b.Tag;
				other.AttackEntity(this, this._damage);
				this.World.RemoveEntity(this);
			}
			else if (b.Tag == this && a.Tag is EntityLiving)
			{
				EntityLiving other = (EntityLiving) a.Tag;
				other.AttackEntity(this, this._damage);
				this.World.RemoveEntity(this);
			}
		}

        protected override Entity NewInstanceForClone()
        {
            return new EntityProjectile(this._ef.Radius, this._ef.Color, this._lifetime, this._damage);
        }

        public override Entity Clone()
        {
            EntityProjectile obj = (EntityProjectile) base.Clone();
            obj._born = this._born;
            return obj;
        }
	}
}
