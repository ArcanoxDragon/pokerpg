using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using GameDotNet.Graphics;
using Jitter;
using Jitter.Collision;
using Jitter.Collision.Shapes;
using Jitter.Dynamics;
using Jitter.LinearMath;
using OpenTK;
using PokeRPG.ScriptEngine;
using PokeRPG.WorldEditor.TypeEditors;

namespace PokeRPG.Entities
{
    public class Entity
    {
        public delegate void WorldEventDelegate(World world);
        public event WorldEventDelegate OnAddedToWorld, OnRemovedFromWorld;

        public World World { get; private set; }
        public RigidBody PhysicsBody { get; protected set; }
        public ScriptedObject Script { get; protected set; }

        public Entity()
        {
            this.World = null;
            this.PhysicsBody = null;

            this.Script = Launcher.GameInstance.ScriptLoader.LoadClass<ScriptedEntity>(string.Format("Scripts/Entities/{0}.cs", this.GetType().Name));
        }

        [Category("Entity")]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Editor(typeof(Vector3TypeEditor), typeof(UITypeEditor))]
        public virtual Vector3 Position
        {
            get
            {
                Vector3 pos = new Vector3(this.PhysicsBody.Position.X, this.PhysicsBody.Position.Y, this.PhysicsBody.Position.Z);
                pos.X -= this.GetWidth();
                pos.Y -= this.GetLength();
                return pos;
            }
            set
            {
                this.PhysicsBody.Position = new JVector(value.X + this.GetWidth(), value.Y + this.GetLength(), value.Z);
            }
        }
        
        public virtual float GetWidth()
        {
            return 0.0f;
        }

        public virtual float GetLength()
        {
            return 0.0f;
        }

        public void AddToWorld(World world)
        {
            this.World = world;
            if (this.OnAddedToWorld != null)
                this.OnAddedToWorld(world);
            if (this.PhysicsBody != null)
                this.World.AddPhysicsBody(this.PhysicsBody);
        }

        public void RemoveFromWorld()
        {
            if (this.PhysicsBody != null)
                this.World.RemovePhysicsBody(this.PhysicsBody);
            if (this.OnRemovedFromWorld != null)
                this.OnRemovedFromWorld(this.World);
            this.World = null;
        }

        public virtual void Update()
        {
            if (this.Script != null)
                this.Script.Update();
        }

        public virtual void Render(Shader shader)
        {
            if (this.Script != null)
                this.Script.Render();
        }

        protected virtual Entity NewInstanceForClone()
        {
            return new Entity();
        }

        public virtual Entity Clone()
        {
            Entity obj = new Entity();
            obj.Script = obj.Script;
            return obj;
        }
    }
}
