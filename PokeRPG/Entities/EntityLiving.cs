﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace PokeRPG.Entities
{
	public class EntityLiving : Entity
	{
		public delegate void EntityDeathHandler(Entity attacker);

		public event EntityDeathHandler OnEntityDeath;

		public float Health { get; protected set; }

		public float MaxHealth { get; protected set; }

		private Entity _lastAttacker;

		public EntityLiving(float maxHealth) : base()
		{
			this.MaxHealth = maxHealth;
			this.Health = this.MaxHealth;
			this._lastAttacker = null;
		}

		public EntityLiving() : this(100.0f) { }

		public void AttackEntity(Entity attacker, float damage)
		{
			this._lastAttacker = attacker;
			if (this.MaxHealth > 0.0f && damage > 0.0f) // Don't take damage if invincible!
			{
				this.Health = Math.Max(0.0f, this.Health - damage);
			}
		}

		public void KillEntity(Entity attacker)
		{
			this.Health = 0.0f;
			if (this.OnEntityDeath != null)
				this.OnEntityDeath(attacker);
			this.World.RemoveEntity(this);
		}

		public void HealEntity(float amount)
		{
			if (amount > 0.0f)
			{
				this.Health = Math.Min(this.MaxHealth, this.Health + amount);
			}
		}

		public override void Update()
		{
			base.Update();
			if (this.Health <= 0.0f && this.MaxHealth > 0.0f)
			{
				this.KillEntity(this._lastAttacker);
			}

			this._lastAttacker = null;
		}

        protected override Entity NewInstanceForClone()
        {
            return new EntityLiving(this.MaxHealth);
        }

        public override Entity Clone()
        {
            EntityLiving obj = (EntityLiving) base.Clone();
            obj.Health = this.Health;
            return obj;
        }
	}
}
