﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK;
using OpenTK.Input;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using PokeRPG.Effects;
using PokeRPG.Entities;
using ObjLoader.Loader.Loaders;
using Jitter;
using Jitter.Collision;
using Jitter.Collision.Shapes;
using JShape = Jitter.Collision.Shapes.Shape;
using Jitter.Dynamics;
using Jitter.LinearMath;
using GameDotNet.Graphics;
using GameDotNet.Audio;
using Shape = GameDotNet.Graphics.Shape;
using PokeRPG.Audio;
using System.Threading;
using System.ComponentModel;
using PokeRPG.WorldEditor.TypeEditors;
using System.Drawing.Design;

namespace PokeRPG.Entities
{
    public class EntityCharacter : EntityLiving
    {
        private const float JUMP_IMPULSE = 4.0f;
        private const float JUMP_HOLD_IMPULSE = 0.1f;
        private const float MAX_Z_VELOCITY = -2.5f;
        private const float PROJECTILE_SPEED = 16.0f;
        private const float MOVE_SPEED = 3.0f;
        private const float MOVE_CONTACT_DISTANCE_X = 0.4f;
        private const float MOVE_CONTACT_DISTANCE_Y = 0.5f;
        private const float FRICTION_COEFFICIENT = 0.85f;
        private const float ROTATION_SPEED = 15.0f;
        private static readonly JVector FORWARD = new JVector(0.0f, 1.0f, 0.0f);
        private static readonly JVector BACKWARD = new JVector(0.0f, -1.0f, 0.0f);
        private static readonly JVector LEFT = new JVector(-1.0f, 0.0f, 0.0f);
        private static readonly JVector RIGHT = new JVector(1.0f, 0.0f, 0.0f);
        private static readonly JVector UP = new JVector(0.0f, 0.0f, 1.0f);
        private static readonly JVector DOWN = new JVector(0.0f, 0.0f, -1.0f);
        private string _modelPath;
        private float _rotation;
        private float _tRot;
        private bool _musicOneShot, _wasJumpDown;
        private Light _light;
        private JShape _shape;
        private int _nextBlink;
        private float _w, _l, _h;
        private RigidBody _dummyBody;
        private JVector _dummyNormal;
        private string _modelName;

        // Cache the position and only update the cache *after* render is complete
        // This helps avoid jitter due to cross-thread desync
        // Partial offset interpolates position between world ticks using velocity
        private JVector _cachePos, _curPartialOffset;

        public EntityCharacter(float x, float y, float z, string modelPath)
            : base()
        {
            this._modelName = modelPath;
            this._rotation = 0.0f;
            this._light = new Light();
            this._light.DiffuseColor = new Color4(1.0f, 0.2f, 1.0f, 1.0f);
            //this._light.AttenuationDistance = 2.0f;
            this._modelPath = modelPath;
            Model body = ModelLibrary.GetModel(modelPath, "Textures/Models", "body");
            JVector modelSize = GeneralUtil.GetModelSize(body);
            ModelLibrary.UnloadModel(body); // Gotta clear this because we're on the wrong thread!
            this._w = Math.Max(modelSize.X, modelSize.Y);
            this._l = Math.Max(modelSize.X, modelSize.Y);
            this._h = modelSize.Z;
            this._shape = new CapsuleShape(this._w, this._w / 2.0f);
            this._nextBlink = GeneralUtil.Random.Next(250, 10000);
            this.PhysicsBody = new RigidBody(this._shape);
            this.PhysicsBody.Position = new JVector(x + (modelSize.X / 2.0f), y + (modelSize.Y / 2.0f), z + (modelSize.Z / 2.0f));
            this.PhysicsBody.IsParticle = true;
            this.PhysicsBody.Tag = this;
            this.PhysicsBody.Material.Restitution = 0.01f;
            this.PhysicsBody.Material.KineticFriction = 0.0f;
            this.PhysicsBody.Material.StaticFriction = 0.0f;
            this.OnAddedToWorld += OnAddToWorld;
            this._musicOneShot = false;
            //this.OnRemovedFromWorld += OnRemoveFromWorld;
            this._cachePos = this.PhysicsBody.Position;
            this._curPartialOffset = JVector.Zero;
        }

        public string ModelName { get { return this._modelName; } }

        public override float GetWidth()
        {
            return this._w;
        }

        public override float GetLength()
        {
            return this._l;
        }

        void OnAddToWorld(World world)
        {
            //this.World.AddLight(this._light);
            this.World.OnMouseClick += OnMouseClick;
            this.World.RenderEvent += WorldRenderEvent;
            this.World.TickEvent += WorldTickEvent;
            this.World.OnBeginCollide += OnCollision;
        }

        void OnCollision(RigidBody a, RigidBody b)
        {
            if (a.Tag == this || b.Tag == this)
            {
                this._curPartialOffset = JVector.Zero; // Collided with something...cancel residual velocity
                this._cachePos = this.PhysicsBody.Position;
            }
        }

        void WorldRenderEvent(EventPhase phase, double interval)
        {
            if (phase == EventPhase.Begin)
            {
                // Only run this on the main render thread! (NOT EDITOR)
                if (Thread.CurrentThread.Name == Launcher.GameInstance.RenderThread.Name)
                {
                    float i = (float) interval;
                    this._cachePos = this.PhysicsBody.Position;
                    this._curPartialOffset.X += this.PhysicsBody.LinearVelocity.X * i;
                    this._curPartialOffset.Y += this.PhysicsBody.LinearVelocity.Y * i;
                    this._curPartialOffset.Z += this.PhysicsBody.LinearVelocity.Z * i;
                    this._cachePos.X += this._curPartialOffset.X;
                    this._cachePos.Y += this._curPartialOffset.Y;
                    this._cachePos.Z += this._curPartialOffset.Z;
                    this.World.CameraX = this._cachePos.X;
                    this.World.CameraY = this._cachePos.Y - 25;
                    this.World.CameraZ = this._cachePos.Z;
                }
            }
        }

        void WorldTickEvent(EventPhase phase, double interval)
        {
            if (phase == EventPhase.End)
            {
                this._curPartialOffset = JVector.Zero;
            }
        }

        void OnRemoveFromWorld(World world)
        {
            this.World.RemoveLight(this._light);
        }

        public override void Update()
        {
            base.Update();

            if (this.World == null)
                return;

            float distance;
            CollisionSystem col = this.World.PhysicsWorld.CollisionSystem;
            KeyboardDevice keys = Launcher.GameInstance.Window.Keyboard;

            if (keys[Key.W])
            {
                if (this.PhysicsBody.LinearVelocity.Y < MOVE_SPEED)
                {
                    col.RemoveEntity(this.PhysicsBody); // Ignore self
                    bool t = col.Raycast(this.PhysicsBody.Position, FORWARD, null, out _dummyBody, out _dummyNormal, out distance);
                    col.AddEntity(this.PhysicsBody);
                    if (!t || distance > MOVE_CONTACT_DISTANCE_Y)
                    {
                        this.PhysicsBody.LinearVelocity += FORWARD;
                    }
                }
                else
                    this.PhysicsBody.LinearVelocity.Set(this.PhysicsBody.LinearVelocity.X, MOVE_SPEED, this.PhysicsBody.LinearVelocity.Z);
                if (!keys[Key.A] && !keys[Key.D])
                    this._tRot = 180f;
            }
            else if (keys[Key.S])
            {
                if (this.PhysicsBody.LinearVelocity.Y > -MOVE_SPEED)
                {
                    col.RemoveEntity(this.PhysicsBody); // Ignore self
                    bool t = col.Raycast(this.PhysicsBody.Position, BACKWARD, null, out _dummyBody, out _dummyNormal, out distance);
                    col.AddEntity(this.PhysicsBody);
                    if (!t || distance > MOVE_CONTACT_DISTANCE_Y)
                    {
                        this.PhysicsBody.LinearVelocity += BACKWARD;
                    }
                }
                else
                    this.PhysicsBody.LinearVelocity.Set(this.PhysicsBody.LinearVelocity.X, -MOVE_SPEED, this.PhysicsBody.LinearVelocity.Z);
                if (!keys[Key.A] && !keys[Key.D])
                    this._tRot = 0f;
            }
            else
            {
                JVector vel = this.PhysicsBody.LinearVelocity;
                vel.Y *= FRICTION_COEFFICIENT;
                if (Math.Abs(vel.Y) < 0.1f)
                    vel.Y = 0.0f;
                this.PhysicsBody.LinearVelocity = vel;
            }
            if (keys[Key.A])
            {
                if (this.PhysicsBody.LinearVelocity.X > -MOVE_SPEED)
                {
                    col.RemoveEntity(this.PhysicsBody); // Ignore self
                    bool t = col.Raycast(this.PhysicsBody.Position, LEFT, null, out _dummyBody, out _dummyNormal, out distance);
                    col.AddEntity(this.PhysicsBody);
                    if (!t || distance > MOVE_CONTACT_DISTANCE_X)
                    {
                        this.PhysicsBody.LinearVelocity += LEFT;
                    }
                }
                else
                    this.PhysicsBody.LinearVelocity.Set(-MOVE_SPEED, this.PhysicsBody.LinearVelocity.Y, this.PhysicsBody.LinearVelocity.Z);
                if (keys[Key.W])
                    this._tRot = 225f;
                else if (keys[Key.S])
                    this._tRot = 315f;
                else
                    this._tRot = 270f;
            }
            else if (keys[Key.D])
            {
                if (this.PhysicsBody.LinearVelocity.X < MOVE_SPEED)
                {
                    col.RemoveEntity(this.PhysicsBody); // Ignore self
                    bool t = col.Raycast(this.PhysicsBody.Position, RIGHT, null, out _dummyBody, out _dummyNormal, out distance);
                    col.AddEntity(this.PhysicsBody);
                    if (!t || distance > MOVE_CONTACT_DISTANCE_X)
                    {
                        this.PhysicsBody.LinearVelocity += RIGHT;
                    }
                }
                else
                    this.PhysicsBody.LinearVelocity.Set(MOVE_SPEED, this.PhysicsBody.LinearVelocity.Y, this.PhysicsBody.LinearVelocity.Z);
                if (keys[Key.W])
                    this._tRot = 135f;
                else if (keys[Key.S])
                    this._tRot = 45f;
                else
                    this._tRot = 90f;
            }
            else
            {
                JVector vel = this.PhysicsBody.LinearVelocity;
                vel.X *= FRICTION_COEFFICIENT;
                if (Math.Abs(vel.X) < 0.1f)
                    vel.X = 0.0f;
                this.PhysicsBody.LinearVelocity = vel;
            }

            if (keys[Key.M])
            {
                if (!this._musicOneShot)
                {
                    this._musicOneShot = true;

                    Soundtrack hometown = SoundtrackLibrary.GetSoundtrack("Cities/Hometown");
                    SoundEngine.Instance.PlaySoundtrack(hometown);
                }
            }
            else
            {
                this._musicOneShot = false;
            }

            if (keys[Key.Space])
            {
                if (!_wasJumpDown)
                {
                    _wasJumpDown = true;
                    col.RemoveEntity(this.PhysicsBody); // Ignore self
                    bool t = col.Raycast(this.PhysicsBody.Position, DOWN, null, out _dummyBody, out _dummyNormal, out distance);
                    col.AddEntity(this.PhysicsBody);
                    if (t && distance - (this._w / 2.0f) < 0.075)
                    {
                        this.PhysicsBody.ApplyImpulse(new JVector(0.0f, 0.0f, JUMP_IMPULSE));
                    }
                }
                else
                {
                    if (this.PhysicsBody.LinearVelocity.Z > 0)
                    {
                        this.PhysicsBody.ApplyImpulse(new JVector(0.0f, 0.0f, JUMP_HOLD_IMPULSE));
                    }
                }
            }
            else
            {
                _wasJumpDown = false;
            }

            if (this.PhysicsBody.LinearVelocity.Z < MAX_Z_VELOCITY)
            {
                JVector linVel = this.PhysicsBody.LinearVelocity;
                linVel.Z = MAX_Z_VELOCITY;
                this.PhysicsBody.LinearVelocity = linVel;
            }

            if (keys[Key.Up])
                this.World.CameraZoom *= 1 + 1f / 128f;
            if (keys[Key.Down])
                this.World.CameraZoom *= 1 - 1f / 128f;

            if (keys[Key.Left])
                this.World.CameraElevation += 1f / 4f;
            if (keys[Key.Right])
                this.World.CameraElevation -= 1f / 4f;

            float aDif = Math.Abs(this._tRot - this._rotation);
            if (aDif < ROTATION_SPEED)
                this._rotation = this._tRot;
            else
            {
                if (aDif < 180f)
                {
                    if (this._rotation < this._tRot)
                        this._rotation += ROTATION_SPEED;
                    else
                        this._rotation -= ROTATION_SPEED;
                }
                else
                {
                    if (this._rotation < this._tRot)
                    {
                        this._rotation -= ROTATION_SPEED;
                        if (this._rotation < 0.0f)
                            this._rotation += 360f;
                    }
                    else
                    {
                        this._rotation += ROTATION_SPEED;
                        if (this._rotation >= 360f)
                            this._rotation -= 360f;
                    }
                }
            }

            this.PhysicsBody.Orientation = JMatrix.CreateFromYawPitchRoll(this._rotation * ((float) Math.PI / 180.0f), 0.0f, 90.0f);

            this._light.Position = new OpenTK.Vector4(this.PhysicsBody.Position.X + (this._w / 2.0f), this.PhysicsBody.Position.Y + (this._l / 2.0f), this.PhysicsBody.Position.Z + 0.5f, 1.0f);
        }

        void OnMouseClick(Vector2 screenPos, Vector3 worldPos, int button, bool didHit, Entity hitEntity, WorldObjects.WorldObject hitObject)
        {
            if (this.World != null)
            {
                //Console.WriteLine("Screen: ({0}, {1}) World: ({2}, {3}, {4}) Button: {5}", screenPos.X, screenPos.Y, worldPos.X, worldPos.Y, worldPos.Z, button);
                if (button == 0)
                {
                    EntityProjectile proj = new EntityProjectile(1f / 4f, new Color4(0.0f, 0.25f, 1.0f, 1.0f), 2000, 0.0f /*12.5f*/);
                    float xOff = (float) Math.Cos((this._rotation - 90) * Math.PI / 180.0f);
                    float yOff = (float) Math.Sin((this._rotation - 90) * Math.PI / 180.0f);
                    proj.PhysicsBody.LinearVelocity = new JVector(PROJECTILE_SPEED * xOff, PROJECTILE_SPEED * yOff, 0.0f);
                    JVector pos = this.PhysicsBody.BoundingBox.Center;
                    pos.X += xOff * 0.75f;
                    pos.Y += yOff * 0.75f;
                    pos.Z += 0.25f;
                    proj.SetPosition(pos.X, pos.Y, pos.Z);
                    this.World.AddEntity(proj);
                }
            }
        }

        public override void Render(Shader shader)
        {
            Model body = ModelLibrary.GetModel(this._modelPath, "Textures/Models", "body");
            Model eyes = ModelLibrary.GetModel(this._modelPath, "Textures/Models", "eyes");

            Transform.PushMatrix();
            Transform.UVPushMatrix();

            Transform.Rotate(MathHelper.DegreesToRadians(this._rotation), 0, 0, 1);
            Transform.Translate(this._cachePos.X, this._cachePos.Y, this._cachePos.Z - (this._h / 2.0f));

            ShaderUtil.SetUniform(shader, "useToon", 1);

            // Body
            body.RenderModel(shader);

            // Eyes
            long millis = GeneralUtil.MillisSinceEpoch();
            if (millis % _nextBlink < 5)
                Transform.UVTranslate(0.0f, 0.25f);
            else if (millis % _nextBlink < 45)
                Transform.UVTranslate(0.0f, 0.5f);
            else if (millis % _nextBlink < 60)
                Transform.UVTranslate(0.0f, 0.25f);
            else if (millis % _nextBlink < 75)
                _nextBlink = GeneralUtil.Random.Next(250, 10000);
            eyes.RenderModel(shader, objectIdOverride: body.ElementBufferId);

            ShaderUtil.SetUniform(shader, "useToon", 0);
            Transform.UVPopMatrix();
            Transform.PopMatrix();
        }

        public override string ToString()
        {
            return "Character (" + this._modelName + ")";
        }

        protected override Entity NewInstanceForClone()
        {
            Vector3 pos = this.Position;
            return new EntityCharacter(pos.X, pos.Y, pos.Z, this.ModelName);
        }

        public override Entity Clone()
        {
            EntityCharacter obj = (EntityCharacter) base.Clone();
            obj._rotation = this._tRot;
            return obj;
        }
    }
}
