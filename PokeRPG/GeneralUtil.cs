﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameDotNet.Graphics;
using Jitter.LinearMath;
using OpenTK;
using OpenTK.Graphics;

namespace PokeRPG
{
    public static class GeneralUtil
    {
        public static readonly TimeSpan ONE_SECOND = new TimeSpan(0, 0, 1);
		public const int SIZE_VEC4 = sizeof(float) * 4;
		public const int SIZE_VEC3 = sizeof(float) * 3;
		public const int SIZE_VEC2 = sizeof(float) * 2;
        public const int SIZE_BUFFER_ENTRY = SIZE_VEC4 + SIZE_VEC3 + SIZE_VEC3 + SIZE_VEC3 + SIZE_VEC2;

        private static readonly DateTime EPOCH = new DateTime(1970, 1, 1);

        public static Random Random { get; private set; }

        static GeneralUtil()
        {
            Random = new Random();
        }

        public static long MillisSinceEpoch()
        {
            return (long) ((DateTime.Now - EPOCH).TotalMilliseconds);
        }

        public static string BytesToString(byte[] bytes)
        {
            string ret = "";
            for (int i = 0; i < bytes.Length; i++)
            {
                if (bytes[i] == '\0')
                    return ret;
                ret += (char) bytes[i];
            }
            return ret;
        }

        public static byte[] StringToBytes(string str)
        {
            byte[] ret = new byte[str.Length + 1];
            ret[ret.Length - 1] = 0;

            for (int i = 0; i < str.Length; i++)
            {
                ret[i] = (byte) str[i];
            }
            return ret;
        }

        /// <summary>
        /// Calculates the three-dimensional size of a model's bounding box
        /// </summary>
        /// <param name="model">Model to use for calculation</param>
        /// <returns>A vector representing the size of the model's bounding box</returns>
        public static JVector GetModelSize(Model model)
        {
            if (model.Vertices.Length <= 0)
                return new JVector(0, 0, 0);
            JVector minVec = new JVector(model.Vertices[0].X, model.Vertices[0].Y, model.Vertices[0].Z);
            JVector maxVec = new JVector(model.Vertices[0].X, model.Vertices[0].Y, model.Vertices[0].Z);
            foreach (Vector4 v in model.Vertices)
            {
                if (v.X < minVec.X)
                    minVec.X = v.X;
                if (v.Y < minVec.Y)
                    minVec.Y = v.Y;
                if (v.Z < minVec.Z)
                    minVec.Z = v.Z;
                if (v.X > maxVec.X)
                    maxVec.X = v.X;
                if (v.Y > maxVec.Y)
                    maxVec.Y = v.Y;
                if (v.Z > maxVec.Z)
                    maxVec.Z = v.Z;
            }
            return maxVec - minVec;
        }

		/// <summary>
		/// Multiplies the specified color by the given scalar.
		/// </summary>
		/// <param name="color">Color to multiply</param>
		/// <param name="scalar">Scalar to multiply by</param>
		/// <returns>A multiplied version of color</returns>
		/// <remarks>Does not multiply Alpha</remarks>
		public static Color4 Multiply(this Color4 color, float scalar)
		{
			return new Color4(color.R * scalar, color.G * scalar, color.B * scalar, color.A);
		}
    }
}
