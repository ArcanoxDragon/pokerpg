﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameDotNet.Audio;

namespace PokeRPG.Audio
{
    public static class SoundtrackLibrary
    {
        private static Dictionary<string, Soundtrack> _soundTracks;

        static SoundtrackLibrary()
        {
            _soundTracks = new Dictionary<string, Soundtrack>();
        }

        public static Soundtrack GetSoundtrack(string name)
        {
            if (!_soundTracks.ContainsKey(name))
            {
                Soundtrack st = SoundUtil.LoadSoundtrackFromName(name);
                if (st.Segments.Count > 0)
                    _soundTracks.Add(name, st);
                else
                    return null;
            }
               
            return _soundTracks[name];
        }
    }
}
