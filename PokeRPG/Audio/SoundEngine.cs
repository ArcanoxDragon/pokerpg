﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using GameDotNet.Audio;

namespace PokeRPG.Audio
{
    public class SoundEngine
    {
        #region Static
        private const int SOUND_ITERATION_RATE = 10;
        private const int SLEEP_TIME = (int) (1000.0f / (float) SOUND_ITERATION_RATE);

        private static SoundEngine _instance;

        public static SoundEngine Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new SoundEngine();
                return _instance;
            }
        }
        #endregion Static

        private enum SoundEngineState
        {
            Stopped,
            Playing,
            Switching
        }

        private Thread _soundThread;
        private bool _running;
        private float _volume;
        private SoundSource _musicSource;
        private Soundtrack _curSoundtrack;
        private Soundtrack _nextSoundtrack;
        private SoundEngineState _curState;
        private float _fadeDelta;

        private SoundEngine()
        {
            this._running = false;
            this._soundThread = null;
            this._volume = 1.0f;
            this._fadeDelta = 1.0f / 32.0f;
            this._curState = SoundEngineState.Stopped;
            this._curSoundtrack = this._nextSoundtrack = null;
        }

        public void Start()
        {
            if (this._running && this._soundThread != null && this._soundThread.ThreadState == ThreadState.Running)
                return;

            if (this._soundThread != null)
                this._soundThread.Abort();
            this._soundThread = new Thread(new ThreadStart(this.ThreadDelegate));
            this._running = true;
            this._soundThread.Start();
        }

        public void Stop()
        {
            this._running = false;
            this._soundThread.Join(1000);
            this._soundThread.Abort();
            this._soundThread = null;
        }

        public void PlaySoundtrack(Soundtrack soundtrack, float fadeDuration = 1.0f)
        {
            this._fadeDelta = 1.0f / ((float) SOUND_ITERATION_RATE * fadeDuration);
            this._nextSoundtrack = soundtrack;
            this._curState = SoundEngineState.Switching;
        }

        private void ThreadDelegate()
        {
            ALUtil.InitAL();
            this._musicSource = new SoundSource();

            while (this._running)
            {
                switch (this._curState)
                {
                    case SoundEngineState.Playing:
                        break;
                    case SoundEngineState.Switching:
                        this._volume -= this._fadeDelta;
                        if (this._volume <= 0.0f || this._curSoundtrack == null)
                        {
                            if (this._curSoundtrack != null)
                                this._curSoundtrack.Stop();
                            this._curSoundtrack = this._nextSoundtrack;
                            this._nextSoundtrack = null;
                            this._volume = 1.0f;
                            if (this._curSoundtrack != null)
                            {
                                this._curSoundtrack.Play(this._musicSource);
                                this._curState = SoundEngineState.Playing;
                            }
                            else
                            {
                                this._curState = SoundEngineState.Stopped;
                            }
                        }
                        break;
                }

                this._volume = Math.Max(Math.Min(this._volume, 1.0f), 0.0f);
                this._musicSource.Volume = this._volume;
                Thread.Sleep(SLEEP_TIME);
            }
        }
    }
}
