﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using GameDotNet.Audio;
using ConfigLibrary;
using GameDotNet;

namespace PokeRPG.Audio
{
    public static class SoundUtil
    {
        public static Soundtrack LoadSoundtrackFromName(string name)
        {
            string path = Path.Combine("Sounds", name);
            string cfgFile = path + ".stf";
            string dir = Path.GetDirectoryName(cfgFile);

            Stream str = null;
            if (File.Exists(cfgFile))
            {
                str = File.Open(cfgFile, FileMode.Open, FileAccess.Read);
            }
            else
            {
                Pack pack = Launcher.GameInstance.Pack;
                if (pack != null && pack.PackFile[cfgFile] != null)
                {
                    str = pack.PackFile[cfgFile].OpenReader();
                }
            }

            if (str != null)
            {
                ConfigFile cfg = new ConfigFile(name, str);
                Soundtrack st = new Soundtrack();
                foreach (string key in cfg.RootNode.Values.Keys)
                {
                    string value;
                    if (cfg.RootNode.GetValue<string>(key, out value))
                    {
                        string[] spl = value.Split(':');
                        if (spl.Length >= 1)
                        {
                            float duration, start = 0.0f;
                            if (float.TryParse(spl[0], out duration) && (spl.Length <= 1 || float.TryParse(spl[1], out start)))
                            {
                                string soundName = key;
                                bool looping = key.ToLower().EndsWith(":l");
                                if (looping)
                                    soundName = soundName.Substring(0, soundName.Length - 2);
                                Sound s = Sound.GetSound(Path.Combine(dir, soundName), start, duration, ".");
                                if (s != null && s.Duration > 0.0f)
                                {
                                    Soundtrack.Segment sg = new Soundtrack.Segment();
                                    sg.type = looping ? Soundtrack.SegmentType.Loop : Soundtrack.SegmentType.PlayOnce;
                                    sg.duration = duration;
                                    sg.sound = s;
                                    st.AddSegment(sg);
                                }
                            }
                        }
                    }
                }
                return st;
            }
            else
            {
                Console.WriteLine("Sound configuration file not found for sound \"{0}\"", name);
                return new Soundtrack();
            }
        }

    }
}
