using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using MTexture = ObjLoader.Loader.Data.VertexData.Texture;
using ObjLoader.Loader.Loaders;
using ObjLoader.Loader.Data;
using ObjLoader.Loader.Data.Elements;
using ObjLoader.Loader.Data.VertexData;
using OpenTK.Graphics;
using Jitter;
using Jitter.Collision;
using Jitter.Collision.Shapes;
using Jitter.Dynamics;
using Jitter.LinearMath;
using GameDotNet.Graphics;

namespace PokeRPG
{
    class JitterDebugDraw : IDebugDrawer
    {
        public static JitterDebugDraw Instance { get; private set; }

        static JitterDebugDraw()
        {
            Instance = new JitterDebugDraw();
        }

        private JitterDebugDraw() { }

        public void DrawLine(JVector start, JVector end)
        {
            return;
        }

        public void DrawPoint(JVector pos)
        {
            return;
        }

        public void DrawTriangle(JVector pos1, JVector pos2, JVector pos3)
        {
            return;
        }
    }

    public static class RenderUtil
    {
        private static float _aspect;
        private static Rectangle _clientRectangle;

        public static void SetView(float zoom, float? aspect = null)
        {
            _clientRectangle = Launcher.GameInstance.Window.ClientRectangle;
            _aspect = aspect ?? (float) _clientRectangle.Width / (float) _clientRectangle.Height;
            Transform.SetProjection(_aspect, MathHelper.PiOver4 / zoom, 1.0f, 100.0f);
        }

        public static void SetCamera(float cameraX, float cameraY, float cameraZ, float elevation)
        {
            Transform.SetCamera(new Vector3(cameraX, cameraY, cameraZ + elevation), new Vector3(cameraX, cameraY + 25, cameraZ), Vector3.UnitZ);
        }

        public static Vector2 WorldToScreen(Vector4 world)
        {
            Vector4 screen = Vector4.Transform(world, Transform.GetInstance().View * Transform.GetInstance().Projection);
            screen.X = screen.X * 0.5f / screen.W + 0.5f;
            screen.Y = screen.Y * 0.5f / screen.W + 0.5f;
            return screen.Xy;
        }

        /*public static Vector3 ScreenToWorld(Vector2 screen, float castDistance)
        {

        }*/

        public static bool IsWorldPointOnScreen(Vector4 world)
        {
            Vector2 screen = WorldToScreen(world);
            return screen.X >= -0.25 && screen.X < 1.25 && screen.Y >= -0.25 && screen.Y < 1.25;
        }
    }
}
