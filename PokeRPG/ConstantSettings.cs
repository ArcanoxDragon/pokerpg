using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PokeRPG
{
    public static class ConstantSettings
    {
        public static string GameName { get { return "PokeRPG"; } }

        public static int GameWidth { get { return 1280; } }
        public static int GameHeight { get { return 720; } }

        public static int TicksPerSecond { get { return 30; } }
        public static int FramesPerSecond { get { return 60; } }

        public static int ShadowMapSize { get { return 4096; } }

    }
}
