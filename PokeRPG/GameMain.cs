using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;
using System.Threading;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using OpenTK.Graphics;
using ObjLoader.Loader.Loaders;
using PokeRPG.ScriptEngine;
using GameDotNet;
using GameDotNet.Graphics;
using System.Drawing;
using System.Windows.Forms;
using PokeRPG.Audio;

namespace PokeRPG
{
    /*public abstract class State
    {
        protected Game Game { get; private set; }

        public virtual void EnterState(Game game)
        {
            this.Game = game;
        }

        public virtual void LeaveState()
        {
            this.Game = null;
        }

        public abstract void Update();

        public abstract void Render();
    }

    public sealed class Game
    {
        private GameWindow _window;
        private State _currentState;
        private bool _shouldRun;
        private object _stateLock;
        private bool _inTick, _inRender, _switchingStates;
        private Thread _renderThread, _tickThread;

        public Game()
        {
            GraphicsMode mode = new GraphicsMode(new ColorFormat(32), 24, 0, 8);
            Console.WriteLine("Starting game at ({0} x {1}) resolution...", Settings.GameWidth, Settings.GameHeight);
            this._window = new GameWindow(Settings.GameWidth, Settings.GameHeight, mode);
            this.ScriptLoader = new ScriptLoader();

            this._window.Closing += OnWindowClosing;
            //this._window.RenderFrame += new EventHandler<FrameEventArgs>(Render);
            //this._window.UpdateFrame += new EventHandler<FrameEventArgs>(Update);
            this._window.Resize += OnResize;
            this._window.VSync = VSyncMode.Off;
            this._stateLock = new object();
        }

        #region Properties

        public double TickPeriod { get; private set; }

        public double TicksPerSecond { get; private set; }

        public double FramePeriod { get; private set; }

        public double FramesPerSecond { get; private set; }

        public bool IsRunning { get; private set; }

        public Pack Pack { get; private set; }

        public GameWindow Window
        {
            get
            {
                return this._window;
            }
        }

        public State State
        {
            get
            {
                return this._currentState;
            }
        }

        public ScriptLoader ScriptLoader { get; private set; }

        #endregion Properties

        public void Start(int tickRate, int maxFrameRate)
        {
            Console.WriteLine("Initializing game...");
            Initialize();

            this._shouldRun = true;
            this.IsRunning = true;
            Console.WriteLine("Initializing OpenGL subsystem...");
            RenderUtil.InitOpenGL();
            this.Window.Context.MakeCurrent(null);

            this._tickThread = new Thread(new ParameterizedThreadStart(TickThread));
            this._renderThread = new Thread(new ParameterizedThreadStart(RenderThread));
            this._tickThread.Start(tickRate);
            this._renderThread.Start(maxFrameRate);

            Console.WriteLine("Game initialized. Displaying main window.");
            this.Window.Visible = true;

            while (this._tickThread.IsAlive || this._renderThread.IsAlive)
            {
                this.Window.ProcessEvents();
                Thread.Sleep(1);
                if (this.Window.IsExiting)
                    this.Kill();
            }

            if (this._currentState != null)
                this._currentState.LeaveState();
            this._currentState = null;

            ShaderUtil.UnloadAllShaders();
            TextureLibrary.UnloadAllLibraries();
            ModelLibrary.UnloadAllModels();

            this.Window.Exit();
            if (WorldEditor.EditorForm.Instance != null)
                WorldEditor.EditorForm.Instance.CloseDetached();
        }

        public void Kill()
        {
            Console.WriteLine("Terminating game");
            this._shouldRun = false;
            this.IsRunning = false;
        }

        public void SwitchState<T>() where T : State, new()
        {
            this._switchingStates = true;
            while (this._inTick)
                Thread.Sleep(10);
            while (this._inRender)
                Thread.Sleep(10);
            if (this._currentState != null)
                this._currentState.LeaveState();
            this._currentState = new T();
            this._currentState.EnterState(this);
            this._switchingStates = false;
        }

        #region Threads
        private void TickThread(object tickRate)
        {
            if (!(tickRate is int))
                return;

            long targetTimePerUpdate = (long) ((1.0 / (double) (int) tickRate) * GeneralUtil.ONE_SECOND.TotalMilliseconds);
            long tickStart, tickLength = targetTimePerUpdate;
            FrameEventArgs e = new FrameEventArgs();

            while (this._shouldRun)
            {
                tickStart = GeneralUtil.MillisSinceEpoch();
                lock (this)
                {
                    this._inTick = true;
                    this.Update(this, e);
                }
                while (this._switchingStates)
                    Thread.Sleep(10);
                tickLength = GeneralUtil.MillisSinceEpoch() - tickStart;
                if (tickLength < targetTimePerUpdate)
                    Thread.Sleep((int) (targetTimePerUpdate - tickLength));
                // Reuse variable and count tick time with sleeping included (for TPS measurement)
                tickLength = GeneralUtil.MillisSinceEpoch() - tickStart;
                this.TickPeriod = ((double) tickLength) / GeneralUtil.ONE_SECOND.TotalMilliseconds;
                this.TicksPerSecond = 1.0 / this.TickPeriod;
                this._inTick = false;
            }
        }

        private void RenderThread(object maxFrameRate)
        {
            if (!(maxFrameRate is int))
                return;

            long targetTimePerFrame = (long) ((1.0 / (double) (int) maxFrameRate) * GeneralUtil.ONE_SECOND.TotalMilliseconds);
            long frameStart, frameLength = targetTimePerFrame;
            FrameEventArgs e = new FrameEventArgs();
            this.Window.MakeCurrent();

            while (this._shouldRun)
            {
                frameStart = GeneralUtil.MillisSinceEpoch();
                //lock (this)
                {
                    this._inRender = true;
                    this.Render(this, e);
                    this.Window.SwapBuffers();
                }
                while (this._switchingStates)
                    Thread.Sleep(10);
                frameLength = GeneralUtil.MillisSinceEpoch() - frameStart;
                if (frameLength <= targetTimePerFrame)
                    Thread.Sleep((int) (targetTimePerFrame - frameLength));
                // Reuse variable and count frame time with sleeping included (for FPS measurement)
                frameLength = GeneralUtil.MillisSinceEpoch() - frameStart;
                this.FramePeriod = ((double) frameLength) / (double) GeneralUtil.ONE_SECOND.TotalMilliseconds;
                this.FramesPerSecond = 1.0 / this.FramePeriod;
                this._inRender = false;
            }
        }
        #endregion

        private void Initialize()
        {
            Console.WriteLine("Loading default pack...");
            this.Pack = new Pack("Default");

            if (this.Pack.IsLoaded)
            {
                Console.WriteLine("Starting state machine");
                SwitchState<WorldState>();

                OnResize(this, null);
            }
            else
            {
                Console.WriteLine("Default pack is not a valid pack! Game cannot continue!");
                this.Kill();
            }
        }

        protected void OnResize(object sender, EventArgs e)
        {
            GL.Viewport(0, 0, this.Window.Width, this.Window.Height);
        }

        private void OnWindowClosing(object sender, CancelEventArgs e)
        {
            e.Cancel = true;
            this.Kill();
        }

        private void Update(object sender, FrameEventArgs e)
        {
            // Update state
            if (this._currentState != null)
                this._currentState.Update();
            // End update state
        }

        private void Render(object sender, FrameEventArgs e)
        {
            // Render state
            if (this._currentState != null)
                this._currentState.Render();
            // End render state
        }
    }*/

    public sealed class PokeRPGGame : Game
    {
        public PokeRPGGame(Settings settings) : base(settings) { this.OnInitialized += PokeRPGGame_OnInitialized; }

        public ScriptLoader ScriptLoader { get; protected set; }

        protected override void Initialize()
        {
            GraphicsContext.ShareContexts = false;
            Console.WriteLine("Loading default pack...");
            this.Pack = new Pack("Default");
            ModelLibrary.SetMaterialProvider(new MaterialProvider());
            this.ScriptLoader = new ScriptLoader();
        }

        void PokeRPGGame_OnInitialized(Game game)
        {
            this.OnClosed += OnGameClosed;
            this.Window.Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath);

            if (this.Pack.IsLoaded && this.Pack.InitializePack())
            {
                Console.WriteLine("Starting sound engine...");
                SoundEngine.Instance.Start();
                Console.WriteLine("Starting state machine");
                SwitchState<WorldState>();
            }
            else
            {
                Console.WriteLine("Default pack is not a valid pack! Game cannot continue!");
                this.Kill();
            }
        }

        void OnGameClosed(Game game)
        {
            SoundEngine.Instance.Stop();
            if (WorldEditor.EditorForm.Instance != null)
                WorldEditor.EditorForm.Instance.CloseDetached();
        }
    }

    sealed class ConsoleLogger : TextWriter
    {
        private TextWriter _parent;
        private StreamWriter _logWriter;

        public ConsoleLogger(string logFile, TextWriter parent)
        {
            this._parent = parent;
            this._logWriter = new StreamWriter(new FileStream(logFile, FileMode.Create));
            this._logWriter.AutoFlush = true;
        }

        public override Encoding Encoding
        {
            get
            {
                return this._parent.Encoding;
            }
        }

        public override void Write(char value)
        {
            this._parent.Write(value);
            this._logWriter.Write(value);
        }
    }

    public static class Launcher
    {
        private static PokeRPGGame _instance;

        public static PokeRPGGame GameInstance
        {
            get
            {
                return Launcher._instance;
            }
        }

        static void Main()
        {
            AppDomain.CurrentDomain.UnhandledException += HandleUnhandledException;
            Console.SetOut(new ConsoleLogger("PokeRPG.log", Console.Out));
            Console.WriteLine("Started logging to \"PokeRPG.log\".");
            Console.WriteLine("Starting game...");
            Settings settings = new Settings();
            settings.FPS = ConstantSettings.FramesPerSecond;
            settings.TPS = ConstantSettings.TicksPerSecond;
            settings.GameName = ConstantSettings.GameName;
            settings.Width = ConstantSettings.GameWidth;
            settings.Height = ConstantSettings.GameHeight;
            settings.ShadowMapSize = ConstantSettings.ShadowMapSize;
            Launcher._instance = new PokeRPGGame(settings);
            Launcher._instance.Start();
        }

        static void HandleUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Console.WriteLine("An unhandled exception has occurred and the game needs to close.");
            Console.WriteLine(e.ExceptionObject.ToString());
            if (GameInstance != null)
                GameInstance.Kill();
        }
    }
}
