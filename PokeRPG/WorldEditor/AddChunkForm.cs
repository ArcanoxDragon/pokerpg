﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PokeRPG.WorldEditor
{
    public partial class AddChunkForm : Form
    {
        private World _world;
        private int _cX, _cY;

        public AddChunkForm(World world)
        {
            InitializeComponent();

            this._world = world;
        }

        public ChunkCoordinate ChunkCoordinate { get { return new ChunkCoordinate(this._cX, this._cY); } }

        private void ValidateTextBox(TextBox textBox)
        {
            for (int i = 0; i < textBox.Text.Length; i++)
            {
                if (!char.IsDigit(textBox.Text[i]) && textBox.Text[i] != '-')
                    textBox.Text = textBox.Text.Substring(0, i) + textBox.Text.Substring(i + 1, textBox.Text.Length - (i + 1));
            }
        }

        private void textBox_TextChanged(object sender, EventArgs e)
        {
            ValidateTextBox((TextBox) sender);

            buttonOK.Enabled = false;
            labelChunkExists.Visible = false;
            if (textBoxX.Text != "" && textBoxY.Text != "")
            {
                if (int.TryParse(textBoxX.Text, out this._cX) && int.TryParse(textBoxY.Text, out this._cY))
                {
                    if (this._world.GetChunk(new ChunkCoordinate(this._cX, this._cY)) == null)
                    {
                        buttonOK.Enabled = true;
                    }
                    else
                    {
                        labelChunkExists.Visible = true;
                    }
                }
            }
        }
    }
}
