﻿namespace PokeRPG.WorldEditor
{
    partial class EditorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ToolStripSeparator tsSep1;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditorForm));
            this.tsContainerMain = new System.Windows.Forms.ToolStripContainer();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.tsContainerObjects = new System.Windows.Forms.ToolStripContainer();
            this.tsViewCategory = new System.Windows.Forms.ToolStrip();
            this.buttonWorldObjects = new System.Windows.Forms.ToolStripButton();
            this.buttonEntities = new System.Windows.Forms.ToolStripButton();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.listViewObjects = new System.Windows.Forms.ListView();
            this.columnName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnLocation = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnGuid = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.glControl = new OpenTK.GLControl();
            this.tsObjects = new System.Windows.Forms.ToolStrip();
            this.buttonAddObject = new System.Windows.Forms.ToolStripButton();
            this.buttonDelete = new System.Windows.Forms.ToolStripButton();
            this.buttonDuplicate = new System.Windows.Forms.ToolStripButton();
            this.buttonHighlightSelected = new System.Windows.Forms.ToolStripButton();
            this.propertyGridObject = new System.Windows.Forms.PropertyGrid();
            this.tsMain = new System.Windows.Forms.ToolStrip();
            this.buttonNew = new System.Windows.Forms.ToolStripButton();
            this.buttonOpen = new System.Windows.Forms.ToolStripButton();
            this.buttonSave = new System.Windows.Forms.ToolStripButton();
            this.menuStripMain = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BottomToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.TopToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.RightToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.LeftToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.ContentPanel = new System.Windows.Forms.ToolStripContentPanel();
            this.labelChunk = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.labelWorld = new System.Windows.Forms.ToolStripLabel();
            this.buttonLights = new System.Windows.Forms.ToolStripButton();
            tsSep1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsContainerMain.ContentPanel.SuspendLayout();
            this.tsContainerMain.TopToolStripPanel.SuspendLayout();
            this.tsContainerMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.tsContainerObjects.BottomToolStripPanel.SuspendLayout();
            this.tsContainerObjects.ContentPanel.SuspendLayout();
            this.tsContainerObjects.TopToolStripPanel.SuspendLayout();
            this.tsContainerObjects.SuspendLayout();
            this.tsViewCategory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tsObjects.SuspendLayout();
            this.tsMain.SuspendLayout();
            this.menuStripMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // tsSep1
            // 
            tsSep1.Name = "tsSep1";
            tsSep1.Size = new System.Drawing.Size(6, 25);
            // 
            // tsContainerMain
            // 
            // 
            // tsContainerMain.ContentPanel
            // 
            this.tsContainerMain.ContentPanel.Controls.Add(this.splitContainer);
            this.tsContainerMain.ContentPanel.Size = new System.Drawing.Size(783, 492);
            this.tsContainerMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tsContainerMain.Location = new System.Drawing.Point(0, 24);
            this.tsContainerMain.Name = "tsContainerMain";
            this.tsContainerMain.Size = new System.Drawing.Size(783, 531);
            this.tsContainerMain.TabIndex = 0;
            this.tsContainerMain.Text = "toolStripContainer1";
            // 
            // tsContainerMain.TopToolStripPanel
            // 
            this.tsContainerMain.TopToolStripPanel.BackColor = System.Drawing.Color.Transparent;
            this.tsContainerMain.TopToolStripPanel.Controls.Add(this.tsMain);
            // 
            // splitContainer
            // 
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.Location = new System.Drawing.Point(0, 0);
            this.splitContainer.Name = "splitContainer";
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.tsContainerObjects);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.propertyGridObject);
            this.splitContainer.Size = new System.Drawing.Size(783, 492);
            this.splitContainer.SplitterDistance = 608;
            this.splitContainer.TabIndex = 1;
            // 
            // tsContainerObjects
            // 
            // 
            // tsContainerObjects.BottomToolStripPanel
            // 
            this.tsContainerObjects.BottomToolStripPanel.Controls.Add(this.tsViewCategory);
            // 
            // tsContainerObjects.ContentPanel
            // 
            this.tsContainerObjects.ContentPanel.Controls.Add(this.splitContainer1);
            this.tsContainerObjects.ContentPanel.Size = new System.Drawing.Size(608, 442);
            this.tsContainerObjects.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tsContainerObjects.Location = new System.Drawing.Point(0, 0);
            this.tsContainerObjects.Name = "tsContainerObjects";
            this.tsContainerObjects.Size = new System.Drawing.Size(608, 492);
            this.tsContainerObjects.TabIndex = 0;
            this.tsContainerObjects.Text = "toolStripContainer1";
            // 
            // tsContainerObjects.TopToolStripPanel
            // 
            this.tsContainerObjects.TopToolStripPanel.Controls.Add(this.tsObjects);
            // 
            // tsViewCategory
            // 
            this.tsViewCategory.Dock = System.Windows.Forms.DockStyle.None;
            this.tsViewCategory.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsViewCategory.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.labelChunk,
            this.buttonWorldObjects,
            this.buttonLights,
            this.toolStripSeparator2,
            this.labelWorld,
            this.buttonEntities});
            this.tsViewCategory.Location = new System.Drawing.Point(0, 0);
            this.tsViewCategory.Name = "tsViewCategory";
            this.tsViewCategory.Size = new System.Drawing.Size(608, 25);
            this.tsViewCategory.Stretch = true;
            this.tsViewCategory.TabIndex = 0;
            // 
            // buttonWorldObjects
            // 
            this.buttonWorldObjects.Checked = true;
            this.buttonWorldObjects.CheckState = System.Windows.Forms.CheckState.Checked;
            this.buttonWorldObjects.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buttonWorldObjects.Image = ((System.Drawing.Image)(resources.GetObject("buttonWorldObjects.Image")));
            this.buttonWorldObjects.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonWorldObjects.Name = "buttonWorldObjects";
            this.buttonWorldObjects.Size = new System.Drawing.Size(83, 22);
            this.buttonWorldObjects.Text = "WorldObjects";
            this.buttonWorldObjects.Click += new System.EventHandler(this.buttonWorldObjects_Click);
            // 
            // buttonEntities
            // 
            this.buttonEntities.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buttonEntities.Image = ((System.Drawing.Image)(resources.GetObject("buttonEntities.Image")));
            this.buttonEntities.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonEntities.Name = "buttonEntities";
            this.buttonEntities.Size = new System.Drawing.Size(49, 22);
            this.buttonEntities.Text = "Entities";
            this.buttonEntities.Click += new System.EventHandler(this.buttonEntities_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.listViewObjects);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.glControl);
            this.splitContainer1.Size = new System.Drawing.Size(608, 442);
            this.splitContainer1.SplitterDistance = 232;
            this.splitContainer1.TabIndex = 0;
            // 
            // listViewObjects
            // 
            this.listViewObjects.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnName,
            this.columnID,
            this.columnLocation,
            this.columnGuid});
            this.listViewObjects.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewObjects.FullRowSelect = true;
            this.listViewObjects.Location = new System.Drawing.Point(0, 0);
            this.listViewObjects.Name = "listViewObjects";
            this.listViewObjects.Size = new System.Drawing.Size(232, 442);
            this.listViewObjects.TabIndex = 3;
            this.listViewObjects.UseCompatibleStateImageBehavior = false;
            this.listViewObjects.View = System.Windows.Forms.View.Details;
            this.listViewObjects.SelectedIndexChanged += new System.EventHandler(this.listViewObjects_SelectedIndexChanged);
            // 
            // columnName
            // 
            this.columnName.Text = "Name";
            this.columnName.Width = 120;
            // 
            // columnID
            // 
            this.columnID.Text = "ID";
            this.columnID.Width = 30;
            // 
            // columnLocation
            // 
            this.columnLocation.Text = "Location";
            this.columnLocation.Width = 86;
            // 
            // columnGuid
            // 
            this.columnGuid.Text = "GUID";
            this.columnGuid.Width = 240;
            // 
            // glControl
            // 
            this.glControl.BackColor = System.Drawing.Color.Black;
            this.glControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.glControl.Location = new System.Drawing.Point(0, 0);
            this.glControl.Name = "glControl";
            this.glControl.Size = new System.Drawing.Size(372, 442);
            this.glControl.TabIndex = 0;
            this.glControl.VSync = false;
            this.glControl.Paint += new System.Windows.Forms.PaintEventHandler(this.glControl_Paint);
            this.glControl.MouseDown += new System.Windows.Forms.MouseEventHandler(this.glControl_MouseDown);
            this.glControl.MouseMove += new System.Windows.Forms.MouseEventHandler(this.glControl_MouseMove);
            this.glControl.MouseUp += new System.Windows.Forms.MouseEventHandler(this.glControl_MouseUp);
            this.glControl.Resize += new System.EventHandler(this.glControl_Resize);
            // 
            // tsObjects
            // 
            this.tsObjects.Dock = System.Windows.Forms.DockStyle.None;
            this.tsObjects.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsObjects.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buttonAddObject,
            this.buttonDelete,
            this.buttonDuplicate,
            tsSep1,
            this.buttonHighlightSelected});
            this.tsObjects.Location = new System.Drawing.Point(0, 0);
            this.tsObjects.Name = "tsObjects";
            this.tsObjects.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.tsObjects.Size = new System.Drawing.Size(608, 25);
            this.tsObjects.Stretch = true;
            this.tsObjects.TabIndex = 0;
            // 
            // buttonAddObject
            // 
            this.buttonAddObject.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonAddObject.Enabled = false;
            this.buttonAddObject.Image = global::PokeRPG.WorldEditor.EditorResources.add;
            this.buttonAddObject.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonAddObject.Name = "buttonAddObject";
            this.buttonAddObject.Size = new System.Drawing.Size(23, 22);
            this.buttonAddObject.Text = "Add Object";
            this.buttonAddObject.Click += new System.EventHandler(this.buttonAddObject_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonDelete.Enabled = false;
            this.buttonDelete.Image = global::PokeRPG.WorldEditor.EditorResources.delete;
            this.buttonDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(23, 22);
            this.buttonDelete.Text = "Delete Selected Object";
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // buttonDuplicate
            // 
            this.buttonDuplicate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonDuplicate.Enabled = false;
            this.buttonDuplicate.Image = global::PokeRPG.WorldEditor.EditorResources.duplicate;
            this.buttonDuplicate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonDuplicate.Name = "buttonDuplicate";
            this.buttonDuplicate.Size = new System.Drawing.Size(23, 22);
            this.buttonDuplicate.Text = "Duplicate Selected";
            this.buttonDuplicate.Click += new System.EventHandler(this.buttonDuplicate_Click);
            // 
            // buttonHighlightSelected
            // 
            this.buttonHighlightSelected.Checked = true;
            this.buttonHighlightSelected.CheckOnClick = true;
            this.buttonHighlightSelected.CheckState = System.Windows.Forms.CheckState.Checked;
            this.buttonHighlightSelected.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonHighlightSelected.Image = ((System.Drawing.Image)(resources.GetObject("buttonHighlightSelected.Image")));
            this.buttonHighlightSelected.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonHighlightSelected.Name = "buttonHighlightSelected";
            this.buttonHighlightSelected.Size = new System.Drawing.Size(23, 22);
            this.buttonHighlightSelected.Text = "Highlight Selected";
            this.buttonHighlightSelected.Click += new System.EventHandler(this.buttonHighlightSelected_Click);
            // 
            // propertyGridObject
            // 
            this.propertyGridObject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertyGridObject.Location = new System.Drawing.Point(0, 0);
            this.propertyGridObject.Name = "propertyGridObject";
            this.propertyGridObject.PropertySort = System.Windows.Forms.PropertySort.Alphabetical;
            this.propertyGridObject.Size = new System.Drawing.Size(171, 492);
            this.propertyGridObject.TabIndex = 0;
            this.propertyGridObject.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this.propertyGridObject_PropertyValueChanged);
            // 
            // tsMain
            // 
            this.tsMain.AutoSize = false;
            this.tsMain.Dock = System.Windows.Forms.DockStyle.None;
            this.tsMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsMain.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.tsMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buttonNew,
            this.buttonOpen,
            this.buttonSave});
            this.tsMain.Location = new System.Drawing.Point(0, 0);
            this.tsMain.Name = "tsMain";
            this.tsMain.Padding = new System.Windows.Forms.Padding(0);
            this.tsMain.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.tsMain.Size = new System.Drawing.Size(783, 39);
            this.tsMain.Stretch = true;
            this.tsMain.TabIndex = 0;
            // 
            // buttonNew
            // 
            this.buttonNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonNew.Image = global::PokeRPG.WorldEditor.EditorResources._new;
            this.buttonNew.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.buttonNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonNew.Name = "buttonNew";
            this.buttonNew.Size = new System.Drawing.Size(36, 36);
            this.buttonNew.Text = "New World Chunk";
            this.buttonNew.Click += new System.EventHandler(this.buttonNew_Click);
            // 
            // buttonOpen
            // 
            this.buttonOpen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonOpen.Image = global::PokeRPG.WorldEditor.EditorResources.open;
            this.buttonOpen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonOpen.Name = "buttonOpen";
            this.buttonOpen.Size = new System.Drawing.Size(36, 36);
            this.buttonOpen.Text = "Open World Chunk";
            this.buttonOpen.Click += new System.EventHandler(this.buttonOpen_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonSave.Image = global::PokeRPG.WorldEditor.EditorResources.save;
            this.buttonSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(36, 36);
            this.buttonSave.Text = "Save World Chunk";
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // menuStripMain
            // 
            this.menuStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStripMain.Location = new System.Drawing.Point(0, 0);
            this.menuStripMain.Name = "menuStripMain";
            this.menuStripMain.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.menuStripMain.Size = new System.Drawing.Size(783, 24);
            this.menuStripMain.TabIndex = 1;
            this.menuStripMain.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.toolStripSeparator1,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.newToolStripMenuItem.Text = "New...";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.openToolStripMenuItem.Text = "Open...";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.buttonOpen_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.saveToolStripMenuItem.Text = "Save...";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(152, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // BottomToolStripPanel
            // 
            this.BottomToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.BottomToolStripPanel.Name = "BottomToolStripPanel";
            this.BottomToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.BottomToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.BottomToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // TopToolStripPanel
            // 
            this.TopToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.TopToolStripPanel.Name = "TopToolStripPanel";
            this.TopToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.TopToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.TopToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // RightToolStripPanel
            // 
            this.RightToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.RightToolStripPanel.Name = "RightToolStripPanel";
            this.RightToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.RightToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.RightToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // LeftToolStripPanel
            // 
            this.LeftToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.LeftToolStripPanel.Name = "LeftToolStripPanel";
            this.LeftToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.LeftToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.LeftToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // ContentPanel
            // 
            this.ContentPanel.Size = new System.Drawing.Size(608, 442);
            // 
            // labelChunk
            // 
            this.labelChunk.Enabled = false;
            this.labelChunk.Name = "labelChunk";
            this.labelChunk.Size = new System.Drawing.Size(42, 22);
            this.labelChunk.Text = "Chunk";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // labelWorld
            // 
            this.labelWorld.Enabled = false;
            this.labelWorld.Name = "labelWorld";
            this.labelWorld.Size = new System.Drawing.Size(39, 22);
            this.labelWorld.Text = "World";
            // 
            // buttonLights
            // 
            this.buttonLights.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buttonLights.Image = ((System.Drawing.Image)(resources.GetObject("buttonLights.Image")));
            this.buttonLights.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonLights.Name = "buttonLights";
            this.buttonLights.Size = new System.Drawing.Size(43, 22);
            this.buttonLights.Text = "Lights";
            this.buttonLights.Click += new System.EventHandler(this.buttonLights_Click);
            // 
            // EditorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(783, 555);
            this.Controls.Add(this.tsContainerMain);
            this.Controls.Add(this.menuStripMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStripMain;
            this.Name = "EditorForm";
            this.Text = "World Editor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.EditorForm_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.EditorForm_FormClosed);
            this.Load += new System.EventHandler(this.EditorForm_Load);
            this.tsContainerMain.ContentPanel.ResumeLayout(false);
            this.tsContainerMain.TopToolStripPanel.ResumeLayout(false);
            this.tsContainerMain.ResumeLayout(false);
            this.tsContainerMain.PerformLayout();
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            this.tsContainerObjects.BottomToolStripPanel.ResumeLayout(false);
            this.tsContainerObjects.BottomToolStripPanel.PerformLayout();
            this.tsContainerObjects.ContentPanel.ResumeLayout(false);
            this.tsContainerObjects.TopToolStripPanel.ResumeLayout(false);
            this.tsContainerObjects.TopToolStripPanel.PerformLayout();
            this.tsContainerObjects.ResumeLayout(false);
            this.tsContainerObjects.PerformLayout();
            this.tsViewCategory.ResumeLayout(false);
            this.tsViewCategory.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tsObjects.ResumeLayout(false);
            this.tsObjects.PerformLayout();
            this.tsMain.ResumeLayout(false);
            this.tsMain.PerformLayout();
            this.menuStripMain.ResumeLayout(false);
            this.menuStripMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripContainer tsContainerMain;
        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.PropertyGrid propertyGridObject;
        private System.Windows.Forms.ToolStrip tsMain;
        private System.Windows.Forms.ToolStripButton buttonOpen;
        private System.Windows.Forms.ToolStripButton buttonNew;
        private System.Windows.Forms.ToolStripButton buttonSave;
        private System.Windows.Forms.MenuStrip menuStripMain;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripContainer tsContainerObjects;
        private System.Windows.Forms.ToolStrip tsViewCategory;
        private System.Windows.Forms.ToolStripButton buttonWorldObjects;
        private System.Windows.Forms.ToolStripButton buttonEntities;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ListView listViewObjects;
        private System.Windows.Forms.ColumnHeader columnName;
        private System.Windows.Forms.ColumnHeader columnID;
        private System.Windows.Forms.ColumnHeader columnLocation;
        private System.Windows.Forms.ColumnHeader columnGuid;
        private OpenTK.GLControl glControl;
        private System.Windows.Forms.ToolStrip tsObjects;
        private System.Windows.Forms.ToolStripButton buttonAddObject;
        private System.Windows.Forms.ToolStripButton buttonDelete;
        private System.Windows.Forms.ToolStripButton buttonDuplicate;
        private System.Windows.Forms.ToolStripButton buttonHighlightSelected;
        private System.Windows.Forms.ToolStripPanel BottomToolStripPanel;
        private System.Windows.Forms.ToolStripPanel TopToolStripPanel;
        private System.Windows.Forms.ToolStripPanel RightToolStripPanel;
        private System.Windows.Forms.ToolStripPanel LeftToolStripPanel;
        private System.Windows.Forms.ToolStripContentPanel ContentPanel;
        private System.Windows.Forms.ToolStripLabel labelChunk;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripLabel labelWorld;
        private System.Windows.Forms.ToolStripButton buttonLights;

    }
}

