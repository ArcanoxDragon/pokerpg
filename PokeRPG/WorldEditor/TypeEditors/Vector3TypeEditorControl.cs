﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OpenTK;
using System.Windows.Forms.Design;

namespace PokeRPG.WorldEditor.TypeEditors
{
    public partial class Vector3TypeEditorControl : UserControl
    {
        private Vector3 _value;
        private IWindowsFormsEditorService _service;

        public Vector3TypeEditorControl(Vector3 value, IWindowsFormsEditorService service)
        {
            InitializeComponent();
            this._value = value;
            this._service = service;
        }

        public Vector3 Value { get { return this._value; } }

        private void ValidateTextBox(TextBox textBox)
        {
            for (int i = 0; i < textBox.Text.Length; i++)
            {
                if (!char.IsDigit(textBox.Text[i]) && textBox.Text[i] != '.' && textBox.Text[i] != '-')
                    textBox.Text = textBox.Text.Substring(0, i) + textBox.Text.Substring(i + 1, textBox.Text.Length - (i + 1));
            }
        }

        private void textBoxX_TextChanged(object sender, EventArgs e)
        {
            ValidateTextBox((TextBox) sender);
            float x;
            if (float.TryParse(textBoxX.Text, out x))
                this._value.X = x;
        }

        private void textBoxY_TextChanged(object sender, EventArgs e)
        {
            ValidateTextBox((TextBox) sender);
            float y;
            if (float.TryParse(textBoxY.Text, out y))
                this._value.Y = y;
        }

        private void textBoxZ_TextChanged(object sender, EventArgs e)
        {
            ValidateTextBox((TextBox) sender);
            float z;
            if (float.TryParse(textBoxZ.Text, out z))
                this._value.Z = z;
        }

        private void Vector3TypeEditorControl_Load(object sender, EventArgs e)
        {
            this.textBoxX.Text = this._value.X.ToString();
            this.textBoxY.Text = this._value.Y.ToString();
            this.textBoxZ.Text = this._value.Z.ToString();
        }
    }
}
