﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using GameDotNet.Graphics;
using OpenTK;
using OpenTK.Graphics;

namespace PokeRPG.WorldEditor.TypeEditors
{
    class LightWrapper
    {
        private Light _light;

        public LightWrapper(Light light)
        {
            this._light = light;
        }

        public Light Light { get { return this._light; } }

        [Category("Light")]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Editor(typeof(Vector4TypeEditor), typeof(UITypeEditor))]
        public Vector4 Position
        {
            get { return this._light.Position; }
            set { this._light.Position = value; }
        }

        [Category("Light")]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Editor(typeof(Color4TypeEditor), typeof(UITypeEditor))]
        public Color4 AmbientColor
        {
            get { return this._light.AmbientColor; }
            set { this._light.AmbientColor = value; }
        }

        [Category("Light")]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Editor(typeof(Color4TypeEditor), typeof(UITypeEditor))]
        public Color4 DiffuseColor
        {
            get { return this._light.DiffuseColor; }
            set { this._light.DiffuseColor = value; }
        }

        [Category("Light")]
        [EditorBrowsable(EditorBrowsableState.Always)]
        public float AttenuationConstant
        {
            get { return this._light.AttenuationConstant; }
            set { this._light.AttenuationConstant = value; }
        }

        [Category("Light")]
        [EditorBrowsable(EditorBrowsableState.Always)]
        public float AttenuationLinear
        {
            get { return this._light.AttenuationLinear; }
            set { this._light.AttenuationLinear = value; }
        }

        [Category("Light")]
        [EditorBrowsable(EditorBrowsableState.Always)]
        public float AttenuationQuadratic
        {
            get { return this._light.AttenuationQuadratic; }
            set { this._light.AttenuationQuadratic = value; }
        }

        [Category("Light")]
        [EditorBrowsable(EditorBrowsableState.Always)]
        public bool CastsShadow
        {
            get { return this._light.Position.W < 0.5f ? this._light.Shadow < 0.5f : false; }
            set
            {
                if (this._light.Position.W < 0.5f)
                    this._light.Shadow = value ? 1.0f : 0.0f;
            }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is LightWrapper) && !(obj is Light))
                return false;
            if (obj is Light)
                return this.Light.Equals(obj);
            else
                return this.Light.Equals(((LightWrapper) obj).Light);
        }

        public override int GetHashCode()
        {
            return this.Light.GetHashCode();
        }

        public static bool operator ==(LightWrapper a, LightWrapper b)
        {
            if ((object) a == null || (object) b == null)
                return ((object) a) == ((object) b);
            return a.Equals(b);
        }

        public static bool operator !=(LightWrapper a, LightWrapper b)
        {
            return !(a == b);
        }
    }
}
