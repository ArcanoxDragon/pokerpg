﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OpenTK;
using System.Windows.Forms.Design;
using OpenTK.Graphics;

namespace PokeRPG.WorldEditor.TypeEditors
{
    public partial class Vector4TypeEditorControl : UserControl
    {
        private Vector4 _value;
        private IWindowsFormsEditorService _service;

        public Vector4TypeEditorControl(Vector4 value, IWindowsFormsEditorService service)
        {
            InitializeComponent();
            this._value = value;
            this._service = service;
        }

        public Vector4 Value { get { return this._value; } }

        private void ValidateTextBox(TextBox textBox)
        {
            for (int i = 0; i < textBox.Text.Length; i++)
            {
                if (!char.IsDigit(textBox.Text[i]) && textBox.Text[i] != '.' && textBox.Text[i] != '-')
                    textBox.Text = textBox.Text.Substring(0, i) + textBox.Text.Substring(i + 1, textBox.Text.Length - (i + 1));
            }
        }

        private void textBoxX_TextChanged(object sender, EventArgs e)
        {
            ValidateTextBox((TextBox) sender);
            float r;
            if (float.TryParse(textBoxX.Text, out r))
                this._value.X = r;
        }

        private void textBoxY_TextChanged(object sender, EventArgs e)
        {
            ValidateTextBox((TextBox) sender);
            float g;
            if (float.TryParse(textBoxY.Text, out g))
                this._value.Y = g;
        }

        private void textBoxZ_TextChanged(object sender, EventArgs e)
        {
            ValidateTextBox((TextBox) sender);
            float b;
            if (float.TryParse(textBoxZ.Text, out b))
                this._value.Z = b;
        }

        private void textBoxA_TextChanged(object sender, EventArgs e)
        {
            ValidateTextBox((TextBox) sender);
            float a;
            if (float.TryParse(textBoxW.Text, out a))
                this._value.W = a;
        }

        private void Vector3TypeEditorControl_Load(object sender, EventArgs e)
        {
            this.textBoxX.Text = this._value.X.ToString();
            this.textBoxY.Text = this._value.Y.ToString();
            this.textBoxZ.Text = this._value.Z.ToString();
            this.textBoxW.Text = this._value.W.ToString();
        }
    }
}
