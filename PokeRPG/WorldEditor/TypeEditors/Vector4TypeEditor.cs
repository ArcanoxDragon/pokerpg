﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Windows.Forms.Design;
using OpenTK;
using OpenTK.Graphics;

namespace PokeRPG.WorldEditor.TypeEditors
{
    class Vector4TypeEditor : UITypeEditor
    {
        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            return UITypeEditorEditStyle.DropDown;
        }

        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            IWindowsFormsEditorService service = null;

            if (!(value is Vector4))
                return Vector4.Zero;

            if (provider != null)
            {
                service = (IWindowsFormsEditorService) provider.GetService(typeof(IWindowsFormsEditorService));
            }

            if (service != null)
            {
                Vector4TypeEditorControl control = new Vector4TypeEditorControl((Vector4) value, service);
                service.DropDownControl(control);
                value = control.Value;
            }

            return value;
        }

    }
}
