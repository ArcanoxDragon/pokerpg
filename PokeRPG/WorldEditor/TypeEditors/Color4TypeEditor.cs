﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Windows.Forms.Design;
using OpenTK;
using OpenTK.Graphics;

namespace PokeRPG.WorldEditor.TypeEditors
{
    class Color4TypeEditor : UITypeEditor
    {
        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            return UITypeEditorEditStyle.DropDown;
        }

        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            IWindowsFormsEditorService service = null;

            if (!(value is Color4))
                return Color4.Black;

            if (provider != null)
            {
                service = (IWindowsFormsEditorService) provider.GetService(typeof(IWindowsFormsEditorService));
            }

            if (service != null)
            {
                Color4TypeEditorControl control = new Color4TypeEditorControl((Color4) value, service);
                service.DropDownControl(control);
                value = control.Value;
            }

            return value;
        }

    }
}
