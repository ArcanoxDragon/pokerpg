﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OpenTK;
using System.Windows.Forms.Design;
using OpenTK.Graphics;

namespace PokeRPG.WorldEditor.TypeEditors
{
    public partial class Color4TypeEditorControl : UserControl
    {
        private Color4 _value;
        private IWindowsFormsEditorService _service;

        public Color4TypeEditorControl(Color4 value, IWindowsFormsEditorService service)
        {
            InitializeComponent();
            this._value = value;
            this._service = service;
        }

        public Color4 Value { get { return this._value; } }

        private void ValidateTextBox(TextBox textBox)
        {
            for (int i = 0; i < textBox.Text.Length; i++)
            {
                if (!char.IsDigit(textBox.Text[i]) && textBox.Text[i] != '.' && textBox.Text[i] != '-')
                    textBox.Text = textBox.Text.Substring(0, i) + textBox.Text.Substring(i + 1, textBox.Text.Length - (i + 1));
            }
        }

        private void textBoxX_TextChanged(object sender, EventArgs e)
        {
            ValidateTextBox((TextBox) sender);
            float r;
            if (float.TryParse(textBoxR.Text, out r))
                this._value.R = r;
        }

        private void textBoxY_TextChanged(object sender, EventArgs e)
        {
            ValidateTextBox((TextBox) sender);
            float g;
            if (float.TryParse(textBoxG.Text, out g))
                this._value.G = g;
        }

        private void textBoxZ_TextChanged(object sender, EventArgs e)
        {
            ValidateTextBox((TextBox) sender);
            float b;
            if (float.TryParse(textBoxB.Text, out b))
                this._value.B = b;
        }

        private void textBoxA_TextChanged(object sender, EventArgs e)
        {
            ValidateTextBox((TextBox) sender);
            float a;
            if (float.TryParse(textBoxA.Text, out a))
                this._value.A = a;
        }

        private void Vector3TypeEditorControl_Load(object sender, EventArgs e)
        {
            this.textBoxR.Text = this._value.R.ToString();
            this.textBoxG.Text = this._value.G.ToString();
            this.textBoxB.Text = this._value.B.ToString();
            this.textBoxA.Text = this._value.A.ToString();
        }
    }
}
