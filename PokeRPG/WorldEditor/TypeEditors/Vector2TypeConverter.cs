﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using OpenTK;

namespace PokeRPG.WorldEditor.TypeEditors
{
    class Vector2TypeConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
                return true;

            return base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
        {
            if (value is string)
            {
                string[] spl = ((string) value).Split(',');

                if (spl.Length == 2)
                {
                    float x, y;
                    if (float.TryParse(spl[0].Trim(), out x) && float.TryParse(spl[1].Trim(), out y))
                        return new Vector2(x, y);
                }
                else
                {
                    throw new InvalidCastException(string.Format("Cannot convert vector of length {0} to Vector2", spl.Length));
                }
            }

            return base.ConvertFrom(context, culture, value);
        }

        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType == typeof(string))
            {

            }
            return base.ConvertTo(context, culture, value, destinationType);
        }
    }
}
