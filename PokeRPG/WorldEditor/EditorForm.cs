﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using GameDotNet;
using GameDotNet.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK;
using PokeRPG;
using PokeRPG.WorldObjects;
using MouseState = OpenTK.Input.MouseState;
using Mouse = OpenTK.Input.Mouse;
using PokeRPG.Entities;
using OpenTK.Graphics;
using PokeRPG.WorldEditor.TypeEditors;

namespace PokeRPG.WorldEditor
{
    public enum EditorCategory : int
    {
        None = -1,
        WorldObjects,
        Lights,
        Entities
    }

    public partial class EditorForm : Form
    {
        private static bool _formOpen = false;
        private static EditorForm _instance = null;
        private static readonly float CAM_SENSITIVITY = 1.0f / 32.0f;
        private static readonly float ZOOM_SENSITIVITY = 1.0f / 64.0f;

        public static EditorForm Instance { get { return _instance; } }

        private EditorCategory _curCategory;
        private World _curWorld;
        private Chunk _curChunk;
        private WorldObject _lastSelectedObject;
        private Entity _lastSelectedEntity;
        private Light _lastSelectedLight;
        private bool _formLoaded = false;
        private Stopwatch _renderTimer;
        private System.Threading.Timer _refreshRenderTimer, _refreshObjectsTimer;
        private Dictionary<MouseButtons, bool> _mbDown;
        private float _camX, _camY, _zoom;
        private float _lastMX, _lastMY;
        private FrameBuffer _fbo = null;
        private bool _didResize = false;
        private MouseState _lastMouse;
        private Light[] _tLights;
        private Light _mainLight;
        private bool _loaded = false;

        public EditorForm()
        {
            InitializeComponent();
            this._tLights = new Light[GLUtil.MAX_LIGHTS];
            this._curCategory = EditorCategory.WorldObjects;
            this._curWorld = GetCurrentWorld();
            this._curChunk = null;
            this._renderTimer = new Stopwatch();
            this._refreshRenderTimer = new System.Threading.Timer(RefreshRender, null, 0, 10);
            this._mbDown = new Dictionary<MouseButtons, bool>();
            this._camX = this._camY = 0.0f;
            this._zoom = 1.0f;

            this._mainLight = new Light();
            this._mainLight.AmbientColor = new Color4(0.5f, 0.5f, 0.5f, 1.0f);
            this._mainLight.DiffuseColor = new Color4(1.0f, 1.0f, 1.0f, 1.0f);
            this._mainLight.Position = new Vector4(-1.0f, -1.0f, -1.0f, 0.0f);

            this._tLights[0] = this._mainLight;
        }

        public Control FindFocusedControl()
        {
            Control control = this.ActiveControl;
            ContainerControl container = control as ContainerControl;
            while (container != null)
            {
                control = container.ActiveControl;
                container = control as ContainerControl;
            }
            return control;
        }

        private void TimerRefreshObjects(object state)
        {
            if (this._curWorld != null)
            {
                this.Invoke(new Action(() =>
                {
                    if (FindFocusedControl() == null || (!FindFocusedControl().GetType().FullName.Contains("PropertyGridInternal.DropDownButton") && !FindFocusedControl().GetType().FullName.Contains("PropertyGridInternal.PropertyGridView+GridViewEdit")))
                        this.RefreshObjects();
                }));
            }
        }

        public void ShowDetached()
        {
            if (_formOpen)
                return;
            _formOpen = true;
            _instance = this;
            Thread thread = new Thread(new ThreadStart(() =>
                {
                    Application.EnableVisualStyles();
                    Application.Run(this);
                }));
            thread.Start();
        }

        public void CloseDetached()
        {
            this.Invoke(new Action(() => { this.Close(); }));
        }

        private World GetCurrentWorld()
        {
            State curState = Launcher.GameInstance.CurrentState;
            if (curState != null && curState is WorldState)
            {
                WorldState worldState = (WorldState) curState;
                return worldState.World;
            }
            return null;
        }

        private void RefreshObjects()
        {
            switch (this._curCategory)
            {
                case EditorCategory.Entities:
                    AddEntitiesToList();
                    break;
                case EditorCategory.WorldObjects:
                    AddWorldObjectsToList();
                    break;
                case EditorCategory.Lights:
                    AddLightsToList();
                    break;
                default:
                    break;
            }

            if (this.listViewObjects.SelectedItems.Count == 1)
            {
                this.propertyGridObject.SelectedObject = this.listViewObjects.SelectedItems[0].Tag;
            }
            else
                this.propertyGridObject.SelectedObject = null;
        }

        private void AddWorldObjectsToList()
        {
            int selIndex = -1;
            if (this.listViewObjects.SelectedItems.Count == 1)
                selIndex = this.listViewObjects.SelectedIndices[0];
            this.listViewObjects.Items.Clear();

            if (this._curChunk != null)
            {
                foreach (WorldObject obj in this._curChunk.GetObjects<WorldObject>())
                {
                    ListViewItem item = new ListViewItem();
                    item.Tag = obj;
                    item.Text = obj.ToString();
                    item.SubItems.Add(new ListViewItem.ListViewSubItem(item, ""));
                    item.SubItems.Add(new ListViewItem.ListViewSubItem(item, obj.GetPosition().ToString()));
                    item.SubItems.Add(new ListViewItem.ListViewSubItem(item, obj.ObjectGuid.ToString()));
                    this.listViewObjects.Items.Add(item);
                }
            }

            if (selIndex >= 0 && selIndex < this.listViewObjects.Items.Count)
            {
                this.listViewObjects.SelectedIndices.Clear();
                this.listViewObjects.SelectedIndices.Add(selIndex);
            }
        }

        private void AddLightsToList()
        {
            int selIndex = -1;
            if (this.listViewObjects.SelectedItems.Count == 1)
                selIndex = this.listViewObjects.SelectedIndices[0];
            this.listViewObjects.Items.Clear();

            if (this._curChunk != null)
            {
                foreach (Light l in this._curChunk.GetLights())
                {
                    ListViewItem item = new ListViewItem();
                    item.Tag = new LightWrapper(l);
                    item.Text = l.ToString();
                    item.SubItems.Add(new ListViewItem.ListViewSubItem(item, ""));
                    item.SubItems.Add(new ListViewItem.ListViewSubItem(item, l.Position.ToString()));
                    this.listViewObjects.Items.Add(item);
                }
            }

            if (selIndex >= 0 && selIndex < this.listViewObjects.Items.Count)
            {
                this.listViewObjects.SelectedIndices.Clear();
                this.listViewObjects.SelectedIndices.Add(selIndex);
            }
        }

        private void AddEntitiesToList()
        {
            int selIndex = -1;
            if (this.listViewObjects.SelectedItems.Count == 1)
                selIndex = this.listViewObjects.SelectedIndices[0];
            this.listViewObjects.Items.Clear();

            foreach (Entity obj in this._curWorld.GetEntities<Entity>())
            {
                ListViewItem item = new ListViewItem();
                item.Tag = obj;
                item.Text = obj.ToString();
                item.SubItems.Add(new ListViewItem.ListViewSubItem(item, ""));
                item.SubItems.Add(new ListViewItem.ListViewSubItem(item, obj.PhysicsBody.Position.ToString()));
                this.listViewObjects.Items.Add(item);
            }

            if (selIndex >= 0 && selIndex < this.listViewObjects.Items.Count)
            {
                this.listViewObjects.SelectedIndices.Clear();
                this.listViewObjects.SelectedIndices.Add(selIndex);
            }
        }

        private void buttonWorldObjects_Click(object sender, EventArgs e)
        {
            if (!buttonWorldObjects.Checked)
            {
                buttonWorldObjects.Checked = true;
                buttonEntities.Checked = false;
                buttonLights.Checked = false;
                this._curCategory = EditorCategory.WorldObjects;
                RefreshObjects();
            }
        }

        private void buttonLights_Click(object sender, EventArgs e)
        {
            if (!buttonLights.Checked)
            {
                buttonWorldObjects.Checked = false;
                buttonEntities.Checked = false;
                buttonLights.Checked = true;
                this._curCategory = EditorCategory.Lights;
                RefreshObjects();
            }
        }

        private void buttonEntities_Click(object sender, EventArgs e)
        {
            if (!buttonEntities.Checked)
            {
                buttonWorldObjects.Checked = false;
                buttonLights.Checked = false;
                buttonEntities.Checked = true;
                this._curCategory = EditorCategory.Entities;
                RefreshObjects();
            }
        }

        private void buttonOpen_Click(object sender, EventArgs e)
        {
            if (this._curWorld == null)
                return;

            GetChunkDialog dlg = new GetChunkDialog(this._curWorld);
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                this._curChunk = dlg.SelectedChunk;
                if (this._curChunk != null)
                {
                    RefreshObjects();
                    buttonAddObject.Enabled = true;
                    buttonDelete.Enabled = true;
                    buttonDuplicate.Enabled = true;
                }
            }
        }

        private void buttonNew_Click(object sender, EventArgs e)
        {
            if (this._curWorld == null)
                return;

            AddChunkForm form = new AddChunkForm(this._curWorld);
            if (form.ShowDialog() == DialogResult.OK)
            {
                ChunkCoordinate cc = form.ChunkCoordinate;
                Chunk ch = new Chunk(this._curWorld, cc);
                this._curWorld.AddChunk(ch);
            }
        }

        private void listViewObjects_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listViewObjects.SelectedItems.Count == 1)
            {
                this.propertyGridObject.SelectedObject = listViewObjects.SelectedItems[0].Tag;
                if (listViewObjects.SelectedItems[0].Tag is WorldObject)
                {
                    WorldObject sel = (WorldObject) listViewObjects.SelectedItems[0].Tag;
                    _lastSelectedEntity = null;
                    if (_lastSelectedObject != null)
                        _lastSelectedObject.Selected = false;
                    if (buttonHighlightSelected.Checked)
                        sel.Selected = true;
                    _lastSelectedObject = sel;
                    _lastSelectedLight = null;
                }
                else if (listViewObjects.SelectedItems[0].Tag is Entity)
                {
                    Entity sel = (Entity) listViewObjects.SelectedItems[0].Tag;
                    if (_lastSelectedObject != null)
                        _lastSelectedObject.Selected = false;
                    _lastSelectedObject = null;
                    _lastSelectedEntity = sel;
                    _lastSelectedLight = null;
                }
                else if (listViewObjects.SelectedItems[0].Tag is LightWrapper)
                {
                    LightWrapper sel = (LightWrapper) listViewObjects.SelectedItems[0].Tag;
                    if (_lastSelectedObject != null)
                        _lastSelectedObject.Selected = false;
                    _lastSelectedObject = null;
                    _lastSelectedEntity = null;
                    _lastSelectedLight = sel.Light;
                }
            }
            else
            {
                this.propertyGridObject.SelectedObject = null;
                if (_lastSelectedObject != null)
                    _lastSelectedObject.Selected = false;
                _lastSelectedObject = null;
                _lastSelectedEntity = null;
                _lastSelectedLight = null;
            }
        }

        private void buttonHighlightSelected_Click(object sender, EventArgs e)
        {
            if (buttonHighlightSelected.Checked)
            {
                if (listViewObjects.SelectedItems.Count == 1)
                {
                    if (listViewObjects.SelectedItems[0].Tag is WorldObject)
                    {
                        WorldObject sel = (WorldObject) listViewObjects.SelectedItems[0].Tag;
                        if (_lastSelectedObject != null)
                            _lastSelectedObject.Selected = false;
                        sel.Selected = true;
                        _lastSelectedObject = sel;
                    }
                }
            }
            else
            {
                if (_lastSelectedObject != null)
                    _lastSelectedObject.Selected = false;
            }
        }

        private void propertyGridObject_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            if (this._curWorld != null)
            {
                RefreshObjects();
            }
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (this._curWorld != null)
            {
                this._curWorld.SaveWorld();
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (this._curWorld != null)
            {
                foreach (ListViewItem item in this.listViewObjects.SelectedItems)
                {
                    if (item.Tag is WorldObject)
                    {
                        WorldObject obj = (WorldObject) item.Tag;
                        obj.Chunk.RemoveObject(obj);
                    }
                    else if (item.Tag is Entity)
                    {
                        Entity entity = (Entity) item.Tag;
                        this._curWorld.RemoveEntity(entity);
                    }
                }
                RefreshObjects();
            }
        }

        private void buttonAddObject_Click(object sender, EventArgs e)
        {
            /*if (this._world != null)
            {
                switch (this._curCategory)
                {
                    case EditorCategory.WorldObjects:
                        AddObjectForm addObject = new AddObjectForm(this._world.Converter.WorldObjectConverters);
                        if (addObject.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {
                            ConvertWorldObjectHandler handler = addObject.ChosenHandler;
                            this._world.AddWorldObject(handler.LoadHandler(handler.ObjectType, new byte[] { }));
                        }
                        break;
                    case EditorCategory.Entities:
                        AddEntityForm addEntity = new AddEntityForm(this._world.Converter.EntityConverters);
                        if (addEntity.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {
                            ConvertEntityHandler handler = addEntity.ChosenHandler;
                            this._world.AddEntity(handler.LoadHandler(handler.ObjectType, new byte[] { }));
                        }
                        break;
                    default:
                        break;
                }
                RefreshObjects();
            }*/
        }

        private void buttonDuplicate_Click(object sender, EventArgs e)
        {
            if (this._curWorld != null)
            {
                foreach (ListViewItem item in this.listViewObjects.SelectedItems)
                {
                    if (item.Tag is WorldObject)
                    {
                        WorldObject obj = (WorldObject) item.Tag;
                        WorldObject newObj = obj.Clone();
                        obj.Chunk.AddObject(newObj);
                    }
                    else if (item.Tag is Entity)
                    {
                        Entity entity = (Entity) item.Tag;
                        Entity newEntity = entity.Clone();
                        this._curWorld.AddEntity(newEntity);
                    }
                    else if (item.Tag is LightWrapper && this._curChunk != null)
                    {
                        LightWrapper lightWrapper = (LightWrapper) item.Tag;
                        Light newLight = lightWrapper.Light.Clone();
                        this._curChunk.AddLight(newLight);
                    }
                }
                RefreshObjects();
            }
        }

        private void EditorForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            _formOpen = false;
            _instance = null;
        }

        private void EditorForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._loaded = false;
            
            this._refreshObjectsTimer.Change(Timeout.Infinite, Timeout.Infinite);
            this._refreshRenderTimer.Change(Timeout.Infinite, Timeout.Infinite);

            if (_lastSelectedObject != null)
                _lastSelectedObject.Selected = false;

            TextureLibrary.UnloadLibrariesForThread();
            ModelLibrary.UnloadCacheForThread();
            ShaderUtil.UnloadCacheForThread();
            Transform.DeleteInstanceForThread();

            if (this._fbo != null)
                this._fbo.Delete();
        }

        private void EditorForm_Load(object sender, EventArgs e)
        {
            this._formLoaded = true;
            this._renderTimer.Start();
            this._refreshObjectsTimer = new System.Threading.Timer(TimerRefreshObjects, null, 0, 2500);
            this._curWorld.OnEntityAdded += EntityAddedOrRemoved;
            this._curWorld.OnEntityRemoved += EntityAddedOrRemoved;
            this._loaded = true;
            GLUtil.InitOpenGL();
        }

        private void EntityAddedOrRemoved(Entity entity)
        {
            if (this._loaded)
            {
                this.Invoke(new Action(this.RefreshObjects));
            }
        }

        private void RenderTranslateHandle(float x, float y, float z, bool highlightX = false, bool highlightY = false, bool highlightZ = false)
        {
            // X axis
            Transform.PushMatrix();
            Transform.Rotate(MathHelper.PiOver2, Vector3.UnitY);
            Transform.Scale(1.0f, 0.05f, 0.05f);
            Transform.Translate(x - 0.025f, y, z);
            ShaderUtil.SetUniform(Shader.CurrentShader, "objColor", Color4.Red.Multiply(highlightX ? 1.5f : 1.0f));
            Shape.CYLINDER.Render();
            Transform.Translate(-(x - 0.025f), -y, -z);
            Transform.Scale(0.25f, 2.0f, 2.0f);
            Transform.Translate(x + 1 - 0.025f, y, z);
            Shape.CONE.Render();
            Transform.PopMatrix();

            // Y axis
            Transform.PushMatrix();
            Transform.Rotate(-MathHelper.PiOver2, Vector3.UnitX);
            Transform.Scale(0.05f, 1.0f, 0.05f);
            Transform.Translate(x, y - 0.025f, z);
            ShaderUtil.SetUniform(Shader.CurrentShader, "objColor", Color4.Lime.Multiply(highlightY ? 1.5f : 1.0f));
            Shape.CYLINDER.Render();
            Transform.Translate(-x, -(y - 0.025f), -z);
            Transform.Scale(2.0f, 0.25f, 2.0f);
            Transform.Translate(x, y + 1 - 0.025f, z);
            Shape.CONE.Render();
            Transform.PopMatrix();

            // Z axis
            Transform.PushMatrix();
            Transform.Scale(0.05f, 0.05f, 1.0f);
            Transform.Translate(x, y, z - 0.025f);
            ShaderUtil.SetUniform(Shader.CurrentShader, "objColor", Color4.Blue.Multiply(highlightZ ? 1.5f : 1.0f));
            Shape.CYLINDER.Render();
            Transform.Translate(-x, -y, -(z - 0.025f));
            Transform.Scale(2.0f, 2.0f, 0.25f);
            Transform.Translate(x, y, z + 1 - 0.025f);
            Shape.CONE.Render();
            Transform.PopMatrix();
        }

        private void RenderEditorGUI()
        {
            ShaderUtil.SetUniform(Shader.CurrentShader, "ambientGlobal", new Color4(0.5f, 0.5f, 0.5f, 1.0f));

            Transform.PushMatrix();
            Transform.LoadIdentity();

            if (this._lastSelectedObject != null)
            {
                RenderTranslateHandle(this._lastSelectedObject.Position.X, this._lastSelectedObject.Position.Y, this._lastSelectedObject.Position.Z);
            }
            else if (this._lastSelectedEntity != null)
            {
                RenderTranslateHandle(this._lastSelectedEntity.PhysicsBody.Position.X, this._lastSelectedEntity.PhysicsBody.Position.Y, this._lastSelectedEntity.PhysicsBody.Position.Z);
            }
            else if (this._lastSelectedLight != null && this._lastSelectedLight.Position.W > 0.5f)
            {
                RenderTranslateHandle(this._lastSelectedLight.Position.X, this._lastSelectedLight.Position.Y, this._lastSelectedLight.Position.Z);
            }

            Transform.PopMatrix();
        }

        private void glControl_Paint(object sender, PaintEventArgs e)
        {
            if (this._formLoaded && this._curWorld != null)
            {
                this.glControl.Context.MakeCurrent(this.glControl.WindowInfo);

                if (this._didResize)
                {
                    this._didResize = false;
                    if (this._fbo != null)
                    {
                        this._fbo.Delete();
                        this._fbo = null;
                    }
                }

                if (this._fbo == null)
                {
                    this._fbo = new FrameBuffer(this.glControl.Width, this.glControl.Height, true, true, true, false);
                    this._fbo.InitializeFBO();
                }

                FboStack.PushFBO(this._fbo);
                GL.DrawBuffers(3, new DrawBuffersEnum[] { DrawBuffersEnum.ColorAttachment0, DrawBuffersEnum.ColorAttachment1, DrawBuffersEnum.ColorAttachment2 });
                Shader worldShader = ShaderUtil.GetShader("Shaders/World/editor");
                Shader screenShader = ShaderUtil.GetShader(WorldState.SCREEN_SHADER_NAME);
                Shader phongShader = ShaderUtil.GetShader("Shaders/Generic/phong");
                worldShader.Bind();

                // Set up toon shading helper
                GLTexture toonStep = TextureLibrary.GetLibrary("Textures").GetTexture("ToonStep");
                GL.ActiveTexture(TextureUnit.Texture20);
                ShaderUtil.SetUniform(worldShader, "toonMap", 20);
                toonStep.Bind();

                RenderUtil.SetView(this._zoom, (float) this.glControl.Width / this.glControl.Height);
                Transform.SetCamera(new Vector3(this._camX, this._camY, 10.0f), new Vector3(this._camX, this._camY, 0.0f), Vector3.UnitY);

                GL.Enable(EnableCap.DepthTest);

                this._curWorld.RenderObjects((float) this._renderTimer.ElapsedMilliseconds / 1000.0f, Shader.CurrentShader, useShadow: false, setCamera: false);
                this._curWorld.RenderEffects((float) this._renderTimer.ElapsedMilliseconds / 1000.0f, Shader.CurrentShader);

                // Editor GUI
                phongShader.Bind();
                GL.Enable(EnableCap.Blend);
                GL.Enable(EnableCap.DepthTest);
                GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
                ShaderUtil.SetUniform(Shader.CurrentShader, "tintColor", new Color4(1.0f, 1.0f, 1.0f, 1.0f));
                RenderEditorGUI();
                ShaderUtil.SetUniform(Shader.CurrentShader, "tintColor", new Color4(0.25f, 0.25f, 0.25f, 0.25f));
                GL.Disable(EnableCap.DepthTest);
                RenderEditorGUI();
                GL.Enable(EnableCap.DepthTest);

                FboStack.PopFBO();
                GL.DrawBuffers(1, new DrawBuffersEnum[] { DrawBuffersEnum.BackLeft });

                // Render framebuffer to screen
                GL.PushAttrib(AttribMask.ViewportBit);
                GL.Disable(EnableCap.DepthTest);
                screenShader.Bind();
                ShaderUtil.SetUniform(screenShader, "scrWidth", this.glControl.Width);
                ShaderUtil.SetUniform(screenShader, "scrHeight", this.glControl.Height);

                GL.ActiveTexture(TextureUnit.Texture0);
                ShaderUtil.SetUniform(screenShader, "fboColor", 0);
                this._fbo.BindColorTexture();

                GL.ActiveTexture(TextureUnit.Texture1);
                ShaderUtil.SetUniform(screenShader, "fboIndex", 1);
                this._fbo.BindIndexTexture();

                GL.ActiveTexture(TextureUnit.Texture2);
                ShaderUtil.SetUniform(screenShader, "fboDepth", 2);
                this._fbo.BindDepthTexture();

                GL.ActiveTexture(TextureUnit.Texture3);
                ShaderUtil.SetUniform(screenShader, "fboGlow", 3);
                this._fbo.BindGlowTexture();

                Shape.PLANE.Render(screenShader);
                GL.Enable(EnableCap.DepthTest);
                GL.PopAttrib();
                // End framebuffer render

                this.glControl.SwapBuffers();
            }
        }

        private void glControl_Resize(object sender, EventArgs e)
        {
            this._didResize = true;
        }

        private void RefreshRender(object state)
        {
            MouseState cur = Mouse.GetState();
            float wDelta = cur.WheelPrecise - _lastMouse.WheelPrecise;
            _lastMouse = cur;
            this._zoom = Math.Max(Math.Min(this._zoom + (wDelta * ZOOM_SENSITIVITY), 2.0f), 0.5f);
            this.glControl.Invalidate();
        }

        private void glControl_MouseDown(object sender, MouseEventArgs e)
        {
            if (!_mbDown.ContainsKey(e.Button))
                _mbDown.Add(e.Button, true);
            else
                _mbDown[e.Button] = true;
            this._lastMX = e.X;
            this._lastMY = e.Y;
        }

        private void glControl_MouseUp(object sender, MouseEventArgs e)
        {
            if (!_mbDown.ContainsKey(e.Button))
                _mbDown.Add(e.Button, false);
            else
                _mbDown[e.Button] = false;
        }

        private void glControl_MouseMove(object sender, MouseEventArgs e)
        {
            bool middle = false;
            bool right = false;
            if (_mbDown.ContainsKey(MouseButtons.Middle))
                middle = _mbDown[MouseButtons.Middle];
            if (_mbDown.ContainsKey(MouseButtons.Left))
                right = _mbDown[MouseButtons.Left];
            if (middle || right)
            {
                this._camX += -(e.X - this._lastMX) * (CAM_SENSITIVITY / this._zoom);
                this._camY += (e.Y - this._lastMY) * (CAM_SENSITIVITY / this._zoom);
                this._lastMX = e.X;
                this._lastMY = e.Y;
            }
        }
    }
}
