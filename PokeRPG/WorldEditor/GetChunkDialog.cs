﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PokeRPG.WorldEditor
{
    public partial class GetChunkDialog : Form
    {
        private World _world;

        public GetChunkDialog(World world)
        {
            InitializeComponent();
            if (world == null)
                this.Close();
            this._world = world;
            this.SelectedChunk = null;
        }

        public Chunk SelectedChunk { get; private set; }

        private void GetChunkDialog_Load(object sender, EventArgs e)
        {
            this.listViewChunks.Items.Clear();

            foreach (Chunk c in this._world.GetChunks())
            {
                ListViewItem item = new ListViewItem();
                item.Text = c.Coordinates.ToString();
                item.Tag = c;
                item.SubItems.Add(new ListViewItem.ListViewSubItem(item, c.GetObjects<WorldObjects.WorldObject>().Count.ToString()));
                this.listViewChunks.Items.Add(item);
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            if (this.listViewChunks.SelectedItems.Count == 1)
            {
                ListViewItem selItem = this.listViewChunks.SelectedItems[0];
                if (selItem != null)
                {
                    if (selItem.Tag != null && selItem.Tag is Chunk)
                    {
                        this.SelectedChunk = (Chunk) selItem.Tag;
                        this.DialogResult = DialogResult.OK;
                    }
                }
            }
        }

        private void listViewChunks_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.listViewChunks.SelectedItems.Count == 1)
                this.buttonOK.Enabled = true;
            else
                this.buttonOK.Enabled = false;
        }

        private void listViewChunks_ItemActivate(object sender, EventArgs e)
        {
            this.buttonOK.PerformClick();
        }
    }
}
