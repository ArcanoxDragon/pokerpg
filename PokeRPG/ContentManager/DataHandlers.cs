﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PokeRPG.WorldObjects;

namespace PokeRPG.ContentManager
{
    public delegate object LoadWorldObjectHandler(World world, Chunk chunk, byte[] data);
    public delegate byte[] SaveWorldItemHandler(World world, Chunk chunk, object obj);
}
