﻿using System;
using System.Collections.Generic;

namespace PokeRPG.ContentManager
{
	public interface IWorldDataHandler
	{
        void RegisterDataTypes(WorldDataTypeRegistry typeList);
	}
}

