﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using PokeRPG.WorldObjects;

namespace PokeRPG.ContentManager
{
    public class WorldDataTypeRegistry
    {
        #region Static
        private static WorldDataTypeRegistry _instance;

        static WorldDataTypeRegistry()
        {
            _instance = new WorldDataTypeRegistry();
        }

        public static WorldDataTypeRegistry Instance { get { return _instance; } }
        #endregion

        private Dictionary<int, LoadWorldObjectHandler> _loadTypes = new Dictionary<int, LoadWorldObjectHandler>();
        private Dictionary<Type, SaveWorldItemHandler> _saveTypes = new Dictionary<Type, SaveWorldItemHandler>();
        private Dictionary<int, Type> _idToType = new Dictionary<int, Type>();
        private Dictionary<Type, int> _typeToId = new Dictionary<Type, int>();

        private WorldDataTypeRegistry()
        {
            this._loadTypes = new Dictionary<int, LoadWorldObjectHandler>();
            this._saveTypes = new Dictionary<Type, SaveWorldItemHandler>();
        }

        public void SearchForDataHandlers()
        {
            this._loadTypes.Clear();
            this._saveTypes.Clear();
            this._idToType.Clear();
            this._typeToId.Clear();

            foreach (Assembly a in AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach (Type t in a.GetTypes())
                {
                    if (t.GetInterfaces().Contains(typeof(IWorldDataHandler)))
                    {
                        IWorldDataHandler handler = (IWorldDataHandler) a.CreateInstance(t.FullName);
                        Console.WriteLine("Registering world load types for class \"{0}\"", t.Name);
                        handler.RegisterDataTypes(this);
                    }
                }
            }
        }

        public void RegisterDataType<E>(int loadType, LoadWorldObjectHandler loadHandler, SaveWorldItemHandler saveHandler) where E : class
        {
            if (this._loadTypes.ContainsKey(loadType) || this._idToType.ContainsKey(loadType) || this._saveTypes.ContainsKey(typeof(E)) || this._typeToId.ContainsKey(typeof(E)))
            {
                throw new Exception(string.Format("Cannot register world data type \"{0}\": already registered", loadType));
            }

            this._loadTypes.Add(loadType, loadHandler);
            this._saveTypes.Add(typeof(E), saveHandler);
            this._idToType.Add(loadType, typeof(E));
            this._typeToId.Add(typeof(E), loadType);
        }

        public LoadWorldObjectHandler GetLoadHandler(int type)
        {
            if (!this._loadTypes.ContainsKey(type))
                throw new Exception(string.Format("World data type ID \"{0}\" has no load handler associated with it.", type));
            return this._loadTypes[type];
        }

        public SaveWorldItemHandler GetSaveHandler(Type type)
        {
            if (!this._saveTypes.ContainsKey(type))
                throw new Exception(string.Format("World data type ID \"{0}\" has no save handler associated with it.", type));
            return this._saveTypes[type];
        }

        public int GetIdForType(Type type)
        {
            if (!this._typeToId.ContainsKey(type))
                throw new Exception(string.Format("World data type \"{0}\" has not been registered.", type.Name));
            return this._typeToId[type];
        }

        public Type GetTypeForId(int id)
        {
            if (!this._idToType.ContainsKey(id))
                throw new Exception(string.Format("World data type ID \"{0}\" has not been registered.", id));
            return this._idToType[id];
        }
    }
}
