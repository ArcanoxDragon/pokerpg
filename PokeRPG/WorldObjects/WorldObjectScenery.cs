﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ObjLoader.Loader.Loaders;
using OpenTK.Graphics.OpenGL;
using PokeRPG.Effects;
using OpenTK.Graphics;
using Jitter;
using Jitter.Collision;
using Jitter.Collision.Shapes;
using Jitter.Dynamics;
using Jitter.LinearMath;
using System.ComponentModel;
using GameDotNet.Graphics;
using OpenTK;

namespace PokeRPG.WorldObjects
{
    public class WorldObjectScenery : WorldObject
    {
        protected string _modelPath;
        protected BoxShape _shape;
        private float _w, _l, _h;
        private bool _sizeValid = false;

        public WorldObjectScenery(float x, float y, float z, string modelPath)
            : base()
        {
            this._modelPath = modelPath;
            Model model = ModelLibrary.GetModel(this._modelPath, "Textures/Models");
            this._shape = new BoxShape(GeneralUtil.GetModelSize(model));
            this.PhysicsBody = new RigidBody(this._shape);
            JVector size = this._shape.BoundingBox.Max - this._shape.BoundingBox.Min;
            ModelLibrary.UnloadModel(model); // Unload model because we're on the wrong thread!
            this._w = size.X;
            this._l = size.Y;
            this._h = size.Z;
            this.PhysicsBody.Position = new JVector(x + (this._w / 2.0f), y + (this._l / 2.0f), z + (this._h / 2.0f));
            this.PhysicsBody.IsStatic = true;
            this.PhysicsBody.Tag = this;
            this.OnAddedToWorld += HandleOnAddedToWorld;
        }

        private void UpdateModelSize()
        {
            float x = this.PhysicsBody.Position.X - (this._w / 2.0f);
            float y = this.PhysicsBody.Position.Y - (this._l / 2.0f);
            float z = this.PhysicsBody.Position.Z - (this._h / 2.0f);
            Model model = ModelLibrary.GetModel(this._modelPath, "Textures/Models");
            this._shape.Size = GeneralUtil.GetModelSize(model);
            JVector size = this._shape.BoundingBox.Max - this._shape.BoundingBox.Min;
            this._w = size.X;
            this._l = size.Y;
            this._h = size.Z;
            this.PhysicsBody.Position = new JVector(x + (this._w / 2.0f), y + (this._l / 2.0f), z + (this._h / 2.0f));
        }

        [Category("Scenery")]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Browsable(true)]
        public string ModelName
        {
            get { return this._modelPath; }
            set
            {
                this._modelPath = value;
                this._sizeValid = false; // force a size update
            }
        }

        public override Vector3 Position
        {
            get
            {
                return base.Position - new Vector3(this._w / 2.0f, this._l / 2.0f, this._h / 2.0f);
            }
            set
            {
                base.Position = value + new Vector3(this._w / 2.0f, this._l / 2.0f, this._h / 2.0f);
            }
        }

        void HandleOnAddedToWorld()
        {
            float chunkX = this.Chunk.Coordinates.X * Chunk.CHUNK_SIZE;
            float chunkY = this.Chunk.Coordinates.Y * Chunk.CHUNK_SIZE;
            this.PhysicsBody.Position += new JVector(chunkX, chunkY, 0.0f);
        }

        public override string GetDisplayName()
        {
            return string.Format("Scenery <{0}>", this._modelPath);
        }

        public override void Render(Shader shader)
        {
            if (!this._sizeValid)
                UpdateModelSize();
            Transform.PushMatrix();
            ShaderUtil.SetUniform(shader, "useToon", 1);
            if (this.Selected)
                ShaderUtil.SetUniform(shader, "tintColor", Color4.Red);
            Transform.Translate(this.PhysicsBody.Position.X - (this._w / 2.0f), this.PhysicsBody.Position.Y - (this._l / 2.0f), this.PhysicsBody.Position.Z - (this._h / 2.0f));
            ModelLibrary.GetModel(this._modelPath, "Textures/Models").RenderModel(shader);
            ShaderUtil.SetUniform(shader, "useToon", 0);
            if (this.Selected)
                ShaderUtil.SetUniform(shader, "tintColor", Color4.White);
            Transform.PopMatrix();
        }

        protected override WorldObject NewInstanceForClone()
        {
            return new WorldObjectScenery(this.Position.X, this.Position.Y, this.Position.Z, this.ModelName);
        }

        public override WorldObject Clone()
        {
            return base.Clone();
        }
    }
}
