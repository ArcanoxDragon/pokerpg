﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK;
using Jitter;
using Jitter.Collision;
using Jitter.Collision.Shapes;
using Jitter.Dynamics;
using Jitter.LinearMath;
using System.ComponentModel;
using PokeRPG.WorldEditor;
using System.Drawing.Design;
using GameDotNet.Graphics;
using PokeRPG.WorldEditor.TypeEditors;

namespace PokeRPG.WorldObjects
{
    public class WorldObject
    {
        protected delegate void WorldObjectDelegate();
        protected event WorldObjectDelegate OnAddedToWorld;
        protected event WorldObjectDelegate OnRemovedFromWorld;

        private Vector3 _nonPhysPosition; // Position variable for if we're not using a physics body

        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public World World { get; private set; }

        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public Chunk Chunk { get; private set; }

        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public RigidBody PhysicsBody { get; protected set; }

        [Category("World Object")]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public Guid ObjectGuid { get; set; }

        [Category("World Object")]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Editor(typeof(Vector3TypeEditor), typeof(UITypeEditor))]
        virtual public Vector3 Position
        {
            get { return this.GetPosition(); }
            set
            {
                if (this.PhysicsBody != null)
                {
                    this.PhysicsBody.Position = new JVector(value.X, value.Y, value.Z);
                }
                else
                {
                    this._nonPhysPosition = value;
                }
            }
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public bool Selected { get; set; }

        public WorldObject()
        {
            this.ObjectGuid = Guid.NewGuid();
            this.World = null;
            this.PhysicsBody = null;
        }

        public void AddToWorld(World world, Chunk chunk)
        {
            this.World = world;
            this.Chunk = chunk;
            if (this.OnAddedToWorld != null)
                this.OnAddedToWorld();
            if (this.PhysicsBody != null)
                this.World.AddPhysicsBody(this.PhysicsBody);
        }

        public void RemoveFromWorld()
        {
            if (this.PhysicsBody != null)
                this.World.RemovePhysicsBody(this.PhysicsBody);
            if (this.OnRemovedFromWorld != null)
                this.OnRemovedFromWorld();
            this.World = null;
            this.Chunk = null;
        }

        public virtual string GetDisplayName()
        {
            return "Generic Object";
        }

        public override string ToString()
        {
            return this.GetDisplayName();
        }

        public Vector3 GetPosition()
        {
            if (this.PhysicsBody == null)
                return this._nonPhysPosition;
            return new Vector3(this.PhysicsBody.Position.X, this.PhysicsBody.Position.Y, this.PhysicsBody.Position.Z);
        }

        public Vector3 GetPositionInChunk()
        {
            if (this.Chunk == null)
                return this.Position;
            return this.Position - new Vector3(this.Chunk.Coordinates.X * Chunk.CHUNK_SIZE, this.Chunk.Coordinates.Y * Chunk.CHUNK_SIZE, 0.0f);
        }

        public virtual void Update()
        {

        }

        public virtual void Render(Shader shader)
        {

        }

        protected virtual WorldObject NewInstanceForClone()
        {
            return new WorldObject();
        }

        public virtual WorldObject Clone()
        {
            WorldObject obj = this.NewInstanceForClone();
            obj.ObjectGuid = Guid.NewGuid();
            obj.Position = this.Position;
            return obj;
        }

    }
}
