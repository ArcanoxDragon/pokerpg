﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PokeRPG.WorldObjects.WorldObjectData
{
    interface ISizeable3D : ISizeable
    {
        double Depth { get; set; }
    }
}
