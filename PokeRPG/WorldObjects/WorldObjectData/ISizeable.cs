﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace PokeRPG.WorldObjects.WorldObjectData
{
    public interface ISizeable
    {
        double Width { get; set; }
        double Height { get; set; }
    }
}
