﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK.Graphics.OpenGL;
using Jitter;
using Jitter.Collision;
using Jitter.Collision.Shapes;
using Jitter.Dynamics;
using Jitter.LinearMath;
using System.ComponentModel;
using OpenTK.Graphics;
using GameDotNet.Graphics;
using Shape = GameDotNet.Graphics.Shape;
using OpenTK;
using PokeRPG.WorldObjects.WorldObjectData;
using PokeRPG.WorldEditor;
using System.Drawing.Design;
using PokeRPG.WorldEditor.TypeEditors;

namespace PokeRPG.WorldObjects
{
    public sealed class WorldObjectGround : WorldObject, ISizeable
    {
        private float _w, _h;
        private string _tex;
        private BoxShape _shape;

        public WorldObjectGround(float x, float y, float z, float width, float height, string textureName)
            : base()
        {
            this._w = width;
            this._h = height;
            this._tex = textureName;
            this._shape = new BoxShape(width, height, 1.0f);
            this.PhysicsBody = new RigidBody(this._shape);
            this.PhysicsBody.IsStatic = true;
#if VERBOSE
            Console.WriteLine("Ground object constructed with parameters: ({0}, {1}, {2}, {3}, {4}, {5})", x, y, z, width, height, textureName);
#endif
            this.PhysicsBody.Position = new JVector(x + (width / 2.0f), y + (height / 2.0f), z - 0.5f);
            this.PhysicsBody.Tag = this;
            this.OnAddedToWorld += HandleOnAddedToWorld;
            this.UVOffset = Vector2.Zero;
            this.UVSize = Vector2.One;
        }

        public override Vector3 Position
        {
            get
            {
                return base.Position - new Vector3(this._w / 2.0f, this._h / 2.0f, -0.5f);
            }
            set
            {
                base.Position = value + new Vector3(this._w / 2.0f, this._h / 2.0f, -0.5f);
            }
        }

        public string TextureName
        {
            get { return this._tex; }
            set { this._tex = value; }
        }

        void HandleOnAddedToWorld()
        {
#if VERBOSE
            Console.WriteLine("Ground object added to world at ({0}, {1}, {2})", this.PhysicsBody.Position.X, this.PhysicsBody.Position.Y, this.PhysicsBody.Position.Z);
#endif
            float chunkX = this.Chunk.Coordinates.X * Chunk.CHUNK_SIZE;
            float chunkY = this.Chunk.Coordinates.Y * Chunk.CHUNK_SIZE;
            this.PhysicsBody.Position += new JVector(chunkX, chunkY, 0.0f);
        }

        public override string GetDisplayName()
        {
            return string.Format("Ground <{0:0.00}, {1:0.00}>", this._w, this._h);
        }

        [Category("World Object")]
        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        public double Width
        {
            get
            {
                return this._w;
            }
            set
            {
                this._w = Math.Max((float) value, 0.0f);
                this._shape.Size = new JVector((float) value, this._shape.Size.Y, this._shape.Size.Z);
            }
        }

        [Category("World Object")]
        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        public double Height
        {
            get
            {
                return this._h;
            }
            set
            {
                this._h = Math.Max((float) value, 0.0f);
                this._shape.Size = new JVector(this._shape.Size.X, (float) value, this._shape.Size.Z);
            }
        }

        [Category("Ground")]
        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Editor(typeof(Vector2TypeEditor), typeof(UITypeEditor))]
        public Vector2 UVOffset { get; set; }

        [Category("Ground")]
        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Editor(typeof(Vector2TypeEditor), typeof(UITypeEditor))]
        public Vector2 UVSize { get; set; }

        public override void Render(Shader shader)
        {
            GL.ActiveTexture(TextureUnit.Texture0);
            TextureLibrary.GetLibrary("Textures/World").GetTexture(this._tex).Bind();

            int iXTile = (int) this._w;
            int iYTile = (int) this._h;
            float tW = this._w / (float) iXTile;
            float tH = this._h / (float) iYTile;

            Transform.PushMatrix();
            Transform.Scale(this._w, this._h, 1.0f);
            Transform.Translate((this.PhysicsBody.Position.X - this._w / 2.0f), (this.PhysicsBody.Position.Y - this._h / 2.0f), this.PhysicsBody.Position.Z + 0.5f);
            Transform.UVPushMatrix();
            Transform.UVScale(iXTile, iYTile);
            Transform.UVScale(this.UVSize);
            Transform.UVTranslate(this.UVOffset);
            if (this.Selected)
                ShaderUtil.SetUniform(shader, "tintColor", Color4.Red);
            Shape.PLANE.Render(shader);
            if (this.Selected)
                ShaderUtil.SetUniform(shader, "tintColor", Color4.White);
            Transform.UVPopMatrix();
            Transform.PopMatrix();
        }

        protected override WorldObject NewInstanceForClone()
        {
            return new WorldObjectGround(this.Position.X, this.Position.Y, this.Position.Z, this._w, this._h, this.TextureName);
        }

        public override WorldObject Clone()
        {
            WorldObjectGround obj = (WorldObjectGround) base.Clone();
            obj.UVOffset = this.UVOffset;
            obj.UVSize = this.UVSize;
            return obj;
        }
    }
}
