using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK;
using OpenTK.Input;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using GameDotNet;
using GameDotNet.Graphics;

namespace PokeRPG
{
    public class WorldState : State
    {
        public static readonly string WORLD_SHADER_NAME = "Shaders/World/world";
        public static readonly string SHADOW_SHADER_NAME = "Shaders/World/shadow";
        public static readonly string SCREEN_SHADER_NAME = "Shaders/Screen/screen";

        private FrameBuffer _fbo;

        private World _world;
        private bool _resized;
        private long _timeBeganResize;

        public World World { get { return this._world; } }

        public override void OnEnterState()
        {
            PokeRPGGame game = (PokeRPGGame) this.Owner;
            game.Window.Resize += OnResize;

            this._world = new World(game.Pack.GetStartWorldName());
            Console.WriteLine("Loading world environment...");
            this._world.LoadWorld();
        }

        void OnResize(object sender, EventArgs e)
        {
            this._resized = true;
            this._timeBeganResize = GeneralUtil.MillisSinceEpoch();
        }

        public override void Update(double interval)
        {
            if (this.Owner.Window.Keyboard[Key.Escape])
                this.Owner.Kill();

            if (this.Owner.Window.Keyboard[Key.F5])
            {
                ShaderUtil.UnloadAllShaders(Launcher.GameInstance.RenderThread);
                TextureLibrary.UnloadAllLibraries(Launcher.GameInstance.RenderThread);
                ModelLibrary.UnloadAllModels(Launcher.GameInstance.RenderThread);
            }

            if (this.Owner.Window.Keyboard[Key.F7])
            {
                if (this._world != null)
                {
                    new WorldEditor.EditorForm().ShowDetached();
                }
            }

            this._world.Update(interval);

            int hours = (int) (this._world.WorldTime * 24.0);
            int minutes = (int) (((this._world.WorldTime * 24.0) - hours) * 60.0);
        }

        public override void Render(double interval)
        {   
            if (this._fbo == null)
            {
                this._fbo = new FrameBuffer(this.Owner.Window.Width, this.Owner.Window.Height, true, true, true, true);
                this._fbo.InitializeFBO();
            }

            if (this._resized)
            {
                if (GeneralUtil.MillisSinceEpoch() - this._timeBeganResize > 250)
                {
                    this._resized = false;
                    this._fbo.Delete();
                    this._fbo = new FrameBuffer(this.Owner.Window.Width, this.Owner.Window.Height, true, true, true, true);
                    this._fbo.InitializeFBO();
                }
            }

            FboStack.PushFBO(this._fbo);
            GL.DrawBuffers(3, new DrawBuffersEnum[] { DrawBuffersEnum.ColorAttachment0, DrawBuffersEnum.ColorAttachment1, DrawBuffersEnum.ColorAttachment2 });
            Shader worldShader = ShaderUtil.GetShader(WorldState.WORLD_SHADER_NAME);
            Shader screenShader = ShaderUtil.GetShader(WorldState.SCREEN_SHADER_NAME);
            worldShader.Bind();

            // Set up toon shading helper
            GLTexture toonStep = TextureLibrary.GetLibrary("Textures").GetTexture("ToonStep");
            GL.ActiveTexture(TextureUnit.Texture20);
            ShaderUtil.SetUniform(worldShader, "toonMap", 20);
            toonStep.Bind();

            // Render scene
            this._world.RenderObjects(interval, worldShader);
            this._world.RenderEffects(interval, worldShader);
            // End render scene

            FboStack.PopFBO();
            GL.DrawBuffers(1, new DrawBuffersEnum[] { DrawBuffersEnum.BackLeft });

            // Render framebuffer to screen
            GL.PushAttrib(AttribMask.ViewportBit);
            GL.Disable(EnableCap.DepthTest);
            screenShader.Bind();
            ShaderUtil.SetUniform(screenShader, "scrWidth", this.Owner.Window.ClientRectangle.Width);
            ShaderUtil.SetUniform(screenShader, "scrHeight", this.Owner.Window.ClientRectangle.Height);

            GL.ActiveTexture(TextureUnit.Texture0);
            ShaderUtil.SetUniform(screenShader, "fboColor", 0);
            this._fbo.BindColorTexture();

            GL.ActiveTexture(TextureUnit.Texture1);
            ShaderUtil.SetUniform(screenShader, "fboIndex", 1);
            this._fbo.BindIndexTexture();

            GL.ActiveTexture(TextureUnit.Texture2);
            ShaderUtil.SetUniform(screenShader, "fboDepth", 2);
            this._fbo.BindDepthTexture();

            GL.ActiveTexture(TextureUnit.Texture3);
            ShaderUtil.SetUniform(screenShader, "fboGlow", 3);
            this._fbo.BindGlowTexture();

            Shape.PLANE.Render(screenShader);
            GL.Enable(EnableCap.DepthTest);
            GL.PopAttrib();
            // End framebuffer render
        }
    }
}
