﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using PokeRPG.WorldObjects;
using PokeRPG.Entities;
using System.Text;
using OpenTK;
using OpenTK.Graphics;
using GameDotNet.Graphics;
using PokeRPG.ContentManager;

namespace PokeRPG
{
    public sealed class DefaultWorldDataHandler : IWorldDataHandler
    {
        public const int ID_SCENERY = 0;
        public const int ID_GROUND = 1;
        public const int ID_CHARACTER = 2;
        public const int ID_LIGHT = 3;

        public void RegisterDataTypes(WorldDataTypeRegistry typeRegistry)
        {
            typeRegistry.RegisterDataType<WorldObjectScenery>(ID_SCENERY, LoadSceneryObject, SaveSceneryObject);
            typeRegistry.RegisterDataType<WorldObjectGround>(ID_GROUND, LoadGroundObject, SaveGroundObject);
            typeRegistry.RegisterDataType<EntityCharacter>(ID_CHARACTER, LoadCharacterObject, SaveCharacterObject);
            typeRegistry.RegisterDataType<Light>(ID_LIGHT, LoadLightObject, SaveLightObject);
        }

        private object LoadSceneryObject(World world, Chunk chunk, byte[] data)
        {
            float oX, oY, oZ;
            oX = BitConverter.ToSingle(data, 0);
            oY = BitConverter.ToSingle(data, 4);
            oZ = BitConverter.ToSingle(data, 8);
            string modelName = GeneralUtil.BytesToString(data.Skip(12).ToArray());
            WorldObjectScenery s = new WorldObjectScenery(oX, oY, oZ, modelName);
            //chunk.AddObject(s);
            return s;
        }

        private byte[] SaveSceneryObject(World world, Chunk chunk, object obj)
        {
            if (!(obj is WorldObjectScenery))
                return new byte[0];
            WorldObjectScenery scenery = (WorldObjectScenery) obj;
            float oX, oY, oZ;
            Vector3 pos = scenery.GetPositionInChunk();
            oX = pos.X;
            oY = pos.Y;
            oZ = pos.Z;
            string modelName = scenery.ModelName;
            List<byte> dataList = new List<byte>();
            dataList.AddRange(BitConverter.GetBytes(oX));
            dataList.AddRange(BitConverter.GetBytes(oY));
            dataList.AddRange(BitConverter.GetBytes(oZ));
            dataList.AddRange(GeneralUtil.StringToBytes(modelName));
            return dataList.ToArray<byte>();
        }

        private object LoadGroundObject(World world, Chunk chunk, byte[] data)
        {
            float gX, gY, gZ;
            float gW, gH;
            float uvOX, uvOY, uvSX, uvSY;
            gX = BitConverter.ToSingle(data, 0);
            gY = BitConverter.ToSingle(data, 4);
            gZ = BitConverter.ToSingle(data, 8);
            gW = BitConverter.ToSingle(data, 12);
            gH = BitConverter.ToSingle(data, 16);
            uvOX = BitConverter.ToSingle(data, 20);
            uvOY = BitConverter.ToSingle(data, 24);
            uvSX = BitConverter.ToSingle(data, 28);
            uvSY = BitConverter.ToSingle(data, 32);
            string textureName = GeneralUtil.BytesToString(data.Skip(36).ToArray());
            WorldObjectGround g = new WorldObjectGround(gX, gY, gZ, gW, gH, textureName);
            g.UVOffset = new Vector2(uvOX, uvOY);
            g.UVSize = new Vector2(uvSX, uvSY);
            //chunk.AddObject(g);
            return g;
        }

        private byte[] SaveGroundObject(World world, Chunk chunk, object obj)
        {
            if (!(obj is WorldObjectGround))
                return new byte[0];
            WorldObjectGround ground = (WorldObjectGround) obj;
            float oX, oY, oZ;
            float gW, gH;
            Vector3 pos = ground.GetPositionInChunk();
            Vector2 uvOffset = ground.UVOffset;
            Vector2 uvSize = ground.UVSize;
            oX = pos.X;
            oY = pos.Y;
            oZ = pos.Z;
            gW = (float) ground.Width;
            gH = (float) ground.Height;
            string texName = ground.TextureName;
            List<byte> dataList = new List<byte>();
            dataList.AddRange(BitConverter.GetBytes(oX));
            dataList.AddRange(BitConverter.GetBytes(oY));
            dataList.AddRange(BitConverter.GetBytes(oZ));
            dataList.AddRange(BitConverter.GetBytes(gW));
            dataList.AddRange(BitConverter.GetBytes(gH));
            dataList.AddRange(BitConverter.GetBytes(uvOffset.X));
            dataList.AddRange(BitConverter.GetBytes(uvOffset.Y));
            dataList.AddRange(BitConverter.GetBytes(uvSize.X));
            dataList.AddRange(BitConverter.GetBytes(uvSize.Y));
            dataList.AddRange(GeneralUtil.StringToBytes(texName));
            return dataList.ToArray<byte>();
        }

        private object LoadCharacterObject(World world, Chunk chunk, byte[] data)
        {
            float oX, oY, oZ;
            oX = BitConverter.ToSingle(data, 0);
            oY = BitConverter.ToSingle(data, 4);
            oZ = BitConverter.ToSingle(data, 8);
            string modelName = GeneralUtil.BytesToString(data.Skip(12).ToArray());
            EntityCharacter eChar = new EntityCharacter(oX, oY, oZ, modelName);
            //world.AddEntity(eChar);
            return eChar;
        }

        private byte[] SaveCharacterObject(World world, Chunk chunk, object obj)
        {
            if (!(obj is EntityCharacter))
                return new byte[0];
            EntityCharacter eChar = (EntityCharacter) obj;
            float oX, oY, oZ;
            Vector3 pos = eChar.Position;
            oX = pos.X;
            oY = pos.Y;
            oZ = pos.Z;
            string modelName = eChar.ModelName;
            List<byte> dataList = new List<byte>();
            dataList.AddRange(BitConverter.GetBytes(oX));
            dataList.AddRange(BitConverter.GetBytes(oY));
            dataList.AddRange(BitConverter.GetBytes(oZ));
            dataList.AddRange(GeneralUtil.StringToBytes(modelName));
            return dataList.ToArray<byte>();
        }

        private object LoadLightObject(World world, Chunk chunk, byte[] data)
        {
            float lX, lY, lZ, lW;
            float dR, dG, dB;
            float aR, aG, aB;
            float aC, aL, aQ;
            lX = BitConverter.ToSingle(data, 0);
            lY = BitConverter.ToSingle(data, 4);
            lZ = BitConverter.ToSingle(data, 8);
            lW = BitConverter.ToSingle(data, 12);
            dR = BitConverter.ToSingle(data, 16);
            dG = BitConverter.ToSingle(data, 20);
            dB = BitConverter.ToSingle(data, 24);
            aR = BitConverter.ToSingle(data, 28);
            aG = BitConverter.ToSingle(data, 32);
            aB = BitConverter.ToSingle(data, 36);
            aC = BitConverter.ToSingle(data, 40);
            aL = BitConverter.ToSingle(data, 44);
            aQ = BitConverter.ToSingle(data, 48);
            Vector4 pos = new Vector4(lX, lY, lZ, lW);
            Color4 dif = new Color4(dR, dG, dB, 1.0f);
            Color4 amb = new Color4(aR, aG, aB, 1.0f);
            Light light = new Light();
            light.Position = pos;
            light.DiffuseColor = dif;
            light.AmbientColor = amb;
            light.AttenuationConstant = aC;
            light.AttenuationLinear = aL;
            light.AttenuationQuadratic = aQ;
            //chunk.AddLight(light);
            return light;
        }

        private byte[] SaveLightObject(World world, Chunk chunk, object obj)
        {
            if (!(obj is Light))
                return new byte[0];
            Light light = (Light) obj;
            float oX, oY, oZ, oW;
            float dR, dG, dB;
            float aR, aG, aB;
            float aC, aL, aQ;
            Vector4 pos = light.Position;
            oX = pos.X;
            oY = pos.Y;
            oZ = pos.Z;
            oW = pos.W;
            dR = light.DiffuseColor.R;
            dG = light.DiffuseColor.G;
            dB = light.DiffuseColor.B;
            aR = light.AmbientColor.R;
            aG = light.AmbientColor.G;
            aB = light.AmbientColor.B;
            aC = light.AttenuationConstant;
            aL = light.AttenuationLinear;
            aQ = light.AttenuationQuadratic;
            List<byte> dataList = new List<byte>();
            dataList.AddRange(BitConverter.GetBytes(oX));
            dataList.AddRange(BitConverter.GetBytes(oY));
            dataList.AddRange(BitConverter.GetBytes(oZ));
            dataList.AddRange(BitConverter.GetBytes(oW));
            dataList.AddRange(BitConverter.GetBytes(dR));
            dataList.AddRange(BitConverter.GetBytes(dG));
            dataList.AddRange(BitConverter.GetBytes(dB));
            dataList.AddRange(BitConverter.GetBytes(aR));
            dataList.AddRange(BitConverter.GetBytes(aG));
            dataList.AddRange(BitConverter.GetBytes(aB));
            dataList.AddRange(BitConverter.GetBytes(aC));
            dataList.AddRange(BitConverter.GetBytes(aL));
            dataList.AddRange(BitConverter.GetBytes(aQ));
            return dataList.ToArray<byte>();
        }
    }
}

