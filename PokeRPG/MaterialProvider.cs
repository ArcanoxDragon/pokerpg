﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ObjLoader.Loader.Loaders;
using System.IO;
using GameDotNet;

namespace PokeRPG
{
    class MaterialProvider : IMaterialStreamProvider
    {
        public Stream Open(string materialFilePath)
        {
            string filename = Path.Combine("Materials", materialFilePath);
            if (!filename.ToLower().EndsWith(".mtl"))
                filename = filename + ".mtl";
            if (File.Exists(filename))
            {
                return new FileStream(filename, FileMode.Open);
            }
            else
            {
                Pack curPack = Launcher.GameInstance.Pack;
                if (curPack.PackFile[filename] != null)
                {
                    return curPack.PackFile[filename].OpenReader();
                }
            }

            return null;
        }
    }
}
